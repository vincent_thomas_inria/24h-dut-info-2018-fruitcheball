@startuml
participant "main:\nMainLaby" as main
participant "lanceur:\nLancerJeuSeq" as lanceur

participant "factorylaby\n:FactoryLabySeq" as factorylaby

create factorylaby
main -> factorylaby: create()

main -> lanceur: create(args, factorylab)
activate lanceur
participant "analyseur:\nAnalyseurOptions" as analyseur

create analyseur
lanceur -> analyseur: create()
lanceur -> analyseur: analyser(args)
activate analyseur


participant "options:Options\n(acces static Singleton)" as options


loop

note right analyseur #lightgreen: analyse args[i] et modifie options de jeu

analyseur -> options: changerOptions(nom,val)
activate options
analyseur <-- options:
deactivate options

end loop




lanceur <-- analyseur:
deactivate analyseur


lanceur -> factorylaby: getJeu()

activate factorylaby
note right: creation du jeu � partir des options connues
factorylaby -> options : getVal(option)
activate options
factorylaby <-- options 
deactivate options

lanceur <-- factorylaby:
deactivate factorylaby

main <-- lanceur:lanceur
deactivate lanceur


main -> lanceur: lancerJeu()
activate lanceur
participant "serveur:\nServeurReseau" as socketServeur

create socketServeur
lanceur -> socketServeur: create()


lanceur -> socketServeur: lancerServeur()
note right #FFAAAA: attente des clients\n(cf diag. attente)
activate socketServeur
lanceur <-- socketServeur:
deactivate socketServeur
note right: tout le monde est connecte


lanceur -> lanceur: creerGui()
note right:creer interface graphique du jeu
activate lanceur
deactivate lanceur


lanceur -> lanceur: executerJeu()
note right #FFAAAA: lance le jeu\n(cf. diag execution)
activate lanceur
deactivate lanceur

main <-- lanceur:
deactivate lanceur


hide footbox
@enduml
