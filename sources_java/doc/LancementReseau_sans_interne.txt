@startuml

participant "main:\nMainServeur" as main

participant "lanceur:\nLancerJeuSeq" as lanceur

participant "factorylaby\n:FactoryLabySeq" as factorylaby
participant "analyseur:\nAnalyseurOptions" as analyseur

participant "options:Options\n(acces static Singleton)" as options

create factorylaby
main -> factorylaby : create()


main -> lanceur: create(args, factorylab)
activate lanceur

lanceur -> factorylaby:initialiserOptions()
activate factorylaby

loop
note right factorylaby #lightgreen: cr�ation des options sp�cifiques au jeu

factorylaby -> options : setOptions(categorie,nom,valeur)
activate options
factorylaby <-- options 
deactivate options

end loop

lanceur <-- factorylaby
deactivate factorylaby




create analyseur
lanceur -> analyseur: create()

lanceur -> analyseur: analyser(args)
activate analyseur




loop

note right analyseur #lightgreen: analyse args[i] et modifie options de jeu

analyseur -> options: changerOptions(nom,valeur)
activate options
analyseur <-- options:
deactivate options

end loop




lanceur <-- analyseur:
deactivate analyseur


lanceur -> factorylaby: getJeu()

activate factorylaby
note right: creation du jeu � partir des options connues
factorylaby -> options : getVal(nom)
activate options
factorylaby <-- options 
deactivate options

participant "jeu:\nJeuSequentiel" as JeuSeq

create JeuSeq
factorylaby -> JeuSeq : create(options)



lanceur <-- factorylaby: jeu
deactivate factorylaby

main <-- lanceur:lanceur
deactivate lanceur

hide footbox

@endUml