@startuml
participant "lanceur:\nlancerJeuSeqReseau" as lanceur

->lanceur: executerJeu()
activate lanceur

lanceur -> lanceur: bouclerJeu()
note right : un tour de jeu
activate lanceur
participant "jeu:\nJeuSequentiel" as jeu

loop

lanceur -> jeu: etreFini()
note right : teste si fin de jeu
activate jeu
lanceur <-- jeu:false
deactivate jeu


lanceur -> lanceur: echangerJoueur(num)
note right : �changes avec joueur en cours\n(indice num)
activate lanceur

lanceur -> jeu: getStatus(num)
note right : construit son descriptif
activate jeu
lanceur <-- jeu:descr
deactivate jeu

participant "serveur:\nServeurReseau" as serveur

lanceur -> serveur: envoyerClient(num,descr)
activate serveur
participant "client[num]:\nClientConnecte" as client_i

serveur-> client_i: envoyerMessage(descr)
activate client_i
serveur<-- client_i:
deactivate client_i

lanceur <-- serveur
deactivate serveur


lanceur -> serveur: demanderClient(num)
activate serveur

serveur -> client_i: recevoirMessage()
activate client_i
serveur <-- client_i: action
deactivate client_i

lanceur <-- serveur:action
deactivate serveur

lanceur <-- lanceur:action
deactivate lanceur


lanceur -> jeu: executerJeu(num,action)
note right: execute le tour de jeu avec action
activate jeu
lanceur <-- jeu:
deactivate jeu



end loop

deactivate lanceur

loop i

lanceur -> lanceur: envoyerFin()
note right: envoi fin de jeu
activate lanceur

lanceur -> serveur : envoyerClient(i,"FIN")
activate serveur

serveur -> client_i : envoyerMessage("FIN")
activate client_i
serveur <-- client_i
deactivate client_i


lanceur <-- serveur
deactivate serveur


deactivate lanceur

end loop

<-- lanceur:
deactivate lanceur

@enduml
