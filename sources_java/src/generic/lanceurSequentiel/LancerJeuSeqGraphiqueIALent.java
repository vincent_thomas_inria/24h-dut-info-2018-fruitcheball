package generic.lanceurSequentiel;

import client.IAClient.FactoryIA;
import generic.analyseurOptions.Options;

/**
 * permet de lancer un jeu avec IA en mode lent (temps d'attente)
 * 
 * @author vthomas
 *
 */
public class LancerJeuSeqGraphiqueIALent extends LancerJeuSeqGraphiqueIA {

	/**
	 * temps d'attente
	 */
	public static int TEMPS =  Options.getOptions().getVal("attente");


	/**
	 * lancement avec un temps d'attente
	 * 
	 * @param args
	 * @param factory
	 * @param fIA
	 */
	public LancerJeuSeqGraphiqueIALent(String[] args, FactoryJeuSeq factory, FactoryIA fIA) {
		super(args, factory, fIA);
	}

	@Override
	public void attendre() {
		try {
			Thread.sleep(TEMPS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
