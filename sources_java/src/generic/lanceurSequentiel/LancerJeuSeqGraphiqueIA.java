package generic.lanceurSequentiel;

import javax.swing.JFrame;

import client.IAClient.FactoryIA;
import client.IAClient.IA;
import generic.lanceurSequentiel.FactoryJeuSeq;
import generic.log.LogSingleton;

 
/**
 * permet de lancer un jeu dans une fenetre mais la lecture des commandes a
 * partir d'une IA fournie par une factoryIA
 */

public class LancerJeuSeqGraphiqueIA extends LancerJeuSeq {

	/**
	 * la frame dans laquelle lancer
	 */
	JFrame frame;

	/**
	 * les IA utilisee pour le jeu
	 */
	IA[] intelligences;

	/**
	 * permet de creer le lanceur de jeu
	 * 
	 * @param j
	 *            jeu a lancer
	 */
	public LancerJeuSeqGraphiqueIA(String[] args, FactoryJeuSeq factory, FactoryIA fIA) {

		
		// recupere le jeu
		super(args, factory);

		// creer les ia
		this.intelligences = new IA[jeu.getNb()];
		for (int i = 0; i < jeu.getNb(); i++) {
			this.intelligences[i] = fIA.getIA();
		}

		// creation de la frame
		frame = new JFrame();
		frame.setContentPane(this.jeu.getVueGraphique());
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	protected String echangerJoueur(int i) {
		// on genere les donnees
		String statut = this.jeu.getStatut(i);
		LogSingleton.ecrire(statut);
		String commande = this.intelligences[i].decider(statut, i);
		LogSingleton.ecrire(commande);
		return (commande);
	}
}
