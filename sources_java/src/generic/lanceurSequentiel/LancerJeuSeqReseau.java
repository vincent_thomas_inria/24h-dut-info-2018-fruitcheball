package generic.lanceurSequentiel;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Observer;
import java.util.Scanner;

import javax.swing.JComponent;
import javax.swing.JFrame;

import generic.analyseurOptions.Options;
import generic.log.LogSingleton;
import generic.outilVue.VuePrinter;
import generic.serveurJeu.ServeurReseau;

/**
 * classe qui permet de lancer le jeu en mode reseau Il attend un nombre de
 * clients egal au nombre de joueurs avant de se lancer.
 * 
 * @author vthomas
 * 
 */
public class LancerJeuSeqReseau extends LancerJeuSeq {

	private static int ATTENTE = Options.getOptions().getVal("attente");

	/**
	 * l'objet en charge de faire le serveur
	 */
	ServeurReseau serveur;

	/**
	 * permet de creer un lanceur reseau avec un jeu
	 * 
	 * @param j
	 *            le jeu a jouer
	 */
	public LancerJeuSeqReseau(String[] args, FactoryJeuSeq jeu) {
		super(args, jeu);
	}

	/**
	 * permet de lancer le jeu via le reseau
	 * 
	 */
	public void lancerJeu() {
		// on met a jour attente
		ATTENTE = Options.getOptions().getVal("attente");

		// on cree le serveur et on attend les connections
		this.serveur = new ServeurReseau(this.jeu.getNb());
		this.serveur.lancerServeur();

		// met a jour les noms envoyes
		String[] nomsClients = this.serveur.getNomsClients();
		for (int i = 0; i < nomsClients.length; i++) {
			String nom = nomsClients[i];
			this.jeu.getJoueurs().get(i).setNom(nom);
		}

		// creer l'interface graphique
		this.creerGUI();

		// toute le monde est connecte => on lance la boucle de jeu
		this.executerJeu();
	}

	/**
	 * permet de creer l'interface graphique grace au observer
	 */
	private void creerGUI() {
		boolean gui = (Options.getOptions().getOption("rendu").getValueInt() == 1);
		if (gui) {
			JFrame frame = new JFrame();
			frame.setContentPane(this.jeu.getVueGraphique());
			frame.pack();
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}

		boolean print = (Options.getOptions().getOption("print").getValueInt() == 1);
		if (print) {
			// recupere la vue
			// demande sauver a chaque pas de temps.
			JComponent vue = this.jeu.getVueGraphique();
			this.jeu.deleteObserver((Observer) vue);
			VuePrinter vp = new VuePrinter("images" + getDate(), vue);
			this.jeu.addObserver(vp);
		}

	}

	/**
	 * permet d'executer le jeu sequentiel via le reseau
	 */
	private void executerJeu() {
		// lance la boucle de jeu
		this.bouclerJeu();

		// envoi la fin pour tous les joueurs
		this.envoyerFin();

		// afficher fin
		LogSingleton.ecrire("***************** FIN***********************");

		// si pas en mode batch
		if (Options.getOptions().getVal("batch") == 0) {
			// on demande la fin a l'utilisateur
			LogSingleton.ecrire("Entree pour arreter");
			Scanner sc = new Scanner(System.in);
			sc.nextLine();
			sc.close();
		}

		// ecrire les scores simplement
		System.out.println("scores finaux:");
		String score = "";
		for (double s : this.jeu.getScore()) {
			score += (int) s + " ";
		}
		System.out.println("" + score);

		// fin du serveur
		System.exit(0);

	}

	/**
	 * envoie la commande fin a tous les joueurs pour signaler la fin du jeu
	 */
	private void envoyerFin() {
		for (int i = 0; i < this.jeu.getNb(); i++) {
			try {
				serveur.envoyerClient(i, "FIN");
			} catch (IOException e) {
				LogSingleton.ecrire("Probleme envoi fin client");
			}
		}
	}

	/**
	 * attendre un temps pour affichage
	 */
	protected void attendre() {
		if (ATTENTE > 0) {
			try {
				Thread.sleep(ATTENTE);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * permet de communiquer avec le joueur numero i
	 * <li>on lui envoie le statut du jeu
	 * <li>on demande sa commande
	 * <li>
	 * 
	 * @param i
	 *            le numero du joueur concerne
	 */
	@Override
	protected String echangerJoueur(int numJoueurCours) {
		LogSingleton.ecrire("contacte joueur " + numJoueurCours);
		try {
			String descriptif = this.jeu.getStatut(numJoueurCours);
			LogSingleton.ecrire("envoie " + numJoueurCours);
			
			// envoyer descriptif au joueur
			serveur.envoyerClient(numJoueurCours, descriptif);

			// attendre reponse et la retourner
			LogSingleton.ecrire("attend " + numJoueurCours);
			String action = serveur.demanderClient(numJoueurCours);
			LogSingleton.ecrire("action recue " + action);
			return (action);

		} catch (IOException e) {
			LogSingleton.ecrire("Probleme de connection client " + numJoueurCours);
			LogSingleton.ecrire("Retourne chaine vide");
			return "";
		}
	}

	/**
	 * permet de construire une date avec secondes
	 * 
	 * @return date construite
	 */
	public static String getDate() {
		String jour = new SimpleDateFormat("dd", Locale.FRANCE).format(new Date());
		String mois = new SimpleDateFormat("MM", Locale.FRANCE).format(new Date());
		String annee = new SimpleDateFormat("yyyy", Locale.FRANCE).format(new Date());
		String heure = new SimpleDateFormat("HH", Locale.FRANCE).format(new Date());
		String minute = new SimpleDateFormat("mm", Locale.FRANCE).format(new Date());
		String seconde = new SimpleDateFormat("ss", Locale.FRANCE).format(new Date());
		return jour + "_" + mois + "_" + annee + "-" + heure + "_" + minute + "_" + seconde;
	}

}
