package generic.outilVue;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * classe outil pour gerer des couleurs a partir d'une echelle de couleur
 * 
 * @author vincent.thomas
 *
 */
public class Couleurs {

	/**
	 * le tableau de couleurs
	 */
	private static List<Color> COULEURS = initialiser();

	/**
	 * retourne la couleur associee au joueur i
	 * 
	 * @param i
	 *            le numero du joueur
	 */
	public static Color getCouleur(int i) {
		// si le joueur n'existe pas, on creer des couleurs aleatoire jusqu'au
		// joueur
		while (COULEURS.size() < i + 1) {
			float random = (float) Math.random();
			COULEURS.add(getCouleurTeinte(random));
		}

		// retourne la couleur
		return COULEURS.get(i);
	}

	/**
	 * construit le tableau des couleurs
	 * 
	 * @return le tableau des couleurs
	 */
	private static List<Color> initialiser() {
		List<Color> tabCol = new ArrayList<>();
		tabCol.add(Color.GREEN);
		tabCol.add(Color.MAGENTA);
		tabCol.add(Color.RED);
		tabCol.add(Color.ORANGE);
		return tabCol;
	}

	/**
	 * retourne la couleur associee a une teinte
	 * 
	 * @param hue
	 *            la teinte souhaitee
	 */
	public static Color getCouleurTeinte(float hue) {
		return new Color(Color.HSBtoRGB(hue, 1f, 1f));
	}

}
