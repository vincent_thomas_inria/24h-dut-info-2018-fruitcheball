package generic.outilVue;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * permet de gerer une vue qui imprime
 */
public class VuePrinter implements Observer {

	/**
	 * la racine pour stocker les images
	 */
	String nomfichier;

	/**
	 * la vue
	 */
	JComponent vue;

	/**
	 * le num de frame
	 */
	int nFrame = 0;

	/**
	 * constructeur
	 * 
	 * @param d
	 *            directory
	 */
	public VuePrinter(String d, JComponent v) {
		File dir = new File(d);
		dir.mkdirs();
		this.nomfichier = d + File.separator;
		this.vue = v;
	}

	@Override
	// la vue printer se charge de sauver les images generees dans l'interface
	public void update(Observable arg0, Object arg1) {
		// creer l'image
		BufferedImage im = new BufferedImage(this.vue.getWidth(), this.vue.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);

		// la passe au paint
		Graphics g = im.getGraphics();
		this.vue.printAll(g);
		g.dispose();

		DecimalFormat format = new DecimalFormat("000");
		String nombre = format.format(nFrame);

		// sauve image
		File f = new File(this.nomfichier + nombre + "_.png");
		try {
			ImageIO.write(im, "png", f);
		} catch (IOException e) {
			// probleme de sauvegarde image
			e.printStackTrace();
		}

		// avance la frame
		nFrame++;

	}
}
