package generic.lanceurSimultane;

import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFrame;

import generic.log.LogSingleton;
import generic.serveurJeu.ServeurReseau;

/**
 * classe qui permet de lancer le jeu en mode reseau Il attend un nombre de
 * clients egal au nombre de joueurs avant de se lancer.
 * 
 * @author vthomas
 *
 */
public class LancerJeuReseau extends LancerJeu {

	private static final int ATTENTE = 100;

	/**
	 * l'objet en charge de faire le serveur
	 */
	ServeurReseau serveur;

	/**
	 * permet de creer un lanceur reseau avec un jeu
	 * 
	 * @param j
	 *            le jeu a jouer
	 */
	public LancerJeuReseau(String[] args, FactoryJeu jeu) {
		super(args, jeu);
	}

	/**
	 * permet de lancer le jeu via le reseau
	 * 
	 */
	public void lancerJeu() {
		// on cree le serveur et on attend les connections
		this.serveur = new ServeurReseau(this.jeu.getNb());
		this.serveur.lancerServeur();

		// met a jour les noms envoyes
		String[] nomsClients = this.serveur.getNomsClients();
		for (int i = 0; i < nomsClients.length; i++) {
			String nom = nomsClients[i];
			this.jeu.getJoueurs().get(i).setNom(nom);
		}

		// creer l'interface graphique
		this.creerGUI();

		// toute le monde est connecte => on lance la boucle de jeu
		this.bouclerJeu();
	}

	/**
	 * permet de creer l'interface graphique grace au observer
	 */
	private void creerGUI() {
		JFrame frame = new JFrame();
		frame.setContentPane(this.jeu.getVueGraphique());
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * permet d'executer le jeu simultane via le reseau
	 */
	private void bouclerJeu() {
		// le temps ecoule
		int t = 0;

		// tant que jeu non fini
		while (!this.jeu.etreFini()) {

			LogSingleton.ecrire(">>> ************** temps : " + t + " **********************");

			// envoyer le nouveau jeu a chaque joueur et demander reponse
			for (int i = 0; i < this.jeu.getNb(); i++) {
				this.communiquerJoueur(i);
			}

			// faire evoluer le jeu
			this.jeu.executerJeu();

			// attente
			this.attendre();
			t++;
		}

		// envoi la fin pour tous les joueurs
		this.envoyerFin();

		// afficher fin
		LogSingleton.ecrire("***************** FIN***********************");
		LogSingleton.ecrire("Entree pour arreter");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
		sc.close();
		System.exit(0);

	}

	/**
	 * envoie la commande fin a tous les joueurs pour signaler la fin du jeu
	 */
	private void envoyerFin() {
		for (int i = 0; i < this.jeu.getNb(); i++) {
			try {
				serveur.envoyerClient(i, "FIN");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(0);
			}
		}
	}

	/**
	 * attendre un temps pour affichage
	 */
	private void attendre() {
		if (ATTENTE > 0) {
			try {

				Thread.sleep(ATTENTE);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * permet de communiquer avec le joueur numero i
	 * <li>on lui envoie le statut du jeu
	 * <li>on demande sa commande
	 * <li>
	 * 
	 * @param i
	 *            le numero du joueur concerne
	 */
	private void communiquerJoueur(int i) {
		try {

			String descriptif = this.jeu.getStatut(i);
			// envoyer descriptif au joueur
			serveur.envoyerClient(i, descriptif);

			// attendre reponse
			String action = serveur.demanderClient(i);
			// mettre a jour la commande du joueur
			this.jeu.determinerCommande(action, i);

		} catch (IOException e) {
			// TODO Il faudrait gerer le cas d'absence de communication en
			// ignorant le joueur
			e.printStackTrace();
			System.exit(0);
		}
	}

}
