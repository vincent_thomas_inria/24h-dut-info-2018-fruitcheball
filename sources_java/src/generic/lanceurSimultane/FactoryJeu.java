package generic.lanceurSimultane;

import generic.jeu.JeuSimultane;

/**
 * classe en charge de generer les options et de construire le jeu
 * 
 * @author vthomas
 *
 */
public abstract class FactoryJeu {

	/**
	 * methode qui a pour objectif d'initialiser les options du jeu
	 */
	public abstract void initialiserOptions();

	/**
	 * methode qui retourne le jeu a lancer
	 */
	public abstract JeuSimultane getJeu();

}
