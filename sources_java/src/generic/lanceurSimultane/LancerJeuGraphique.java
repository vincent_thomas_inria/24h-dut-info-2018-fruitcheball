package generic.lanceurSimultane;

import javax.swing.JFrame;

/**
 * permet de lancer un jeu dans une fenetre mais la lecture des commandes se
 * fait toujours au clavier
 */
public class LancerJeuGraphique extends LancerJeu {

	/**
	 * la frame dans laquelle lancer
	 */
	JFrame frame;

	/**
	 * permet de creer le lanceur de jeu
	 * 
	 * @param j
	 *            jeu a lancer
	 */
	public LancerJeuGraphique(String[] args, FactoryJeu factory) {
		// recupere le jeu
		super(args, factory);

		// creation de la frame
		frame = new JFrame();
		frame.setContentPane(this.jeu.getVueGraphique());
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
