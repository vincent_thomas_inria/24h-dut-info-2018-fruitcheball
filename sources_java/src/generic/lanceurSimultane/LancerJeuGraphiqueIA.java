package generic.lanceurSimultane;

import javax.swing.JFrame;

import client.IAClient.FactoryIA;
import client.IAClient.IA;
import generic.log.LogSingleton;

/**
 * permet de lancer un jeu en mode graphique avec des IA (eventuellement des IA
 * joueurs)
 */
public class LancerJeuGraphiqueIA extends LancerJeu {

	/**
	 * la frame dans laquelle lancer
	 */
	JFrame frame;

	/**
	 * les IA utilisee pour le jeu
	 */
	IA[] intelligences;

	/**
	 * permet de creer le lanceur de jeu
	 * 
	 * @param j
	 *            jeu a lancer
	 */
	public LancerJeuGraphiqueIA(String[] args, FactoryJeu factory, FactoryIA fIA) {
		// creation du jeu
		super(args, factory);

		// creer les ia
		this.intelligences = new IA[jeu.getNb()];
		for (int i = 0; i < jeu.getNb(); i++) {
			this.intelligences[i] = fIA.getIA();
		}

		// la taille du tableau d'ia doit correspondre
		if (this.intelligences.length != jeu.getNb())
			throw new Error("pas bon nombre IAs " + jeu.getNb() + " joueurs " + this.intelligences.length + " IA");

		// creation de la frame
		frame = new JFrame();
		frame.setContentPane(this.jeu.getVueGraphique());
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	protected void demanderJoueur(int i) {
		// TODO moyen de refactorer avec (a) une IA joueur et (b) une IA IA
		// le joueur peut etre vu comme une ia particulier avec un scanner

		// on genere les donnees
		String statut = this.jeu.getStatut(i);
		LogSingleton.ecrire(statut);
		String commande = this.intelligences[i].decider(statut, i);
		LogSingleton.ecrire(commande);
		this.jeu.determinerCommande(commande, i);
	}

}
