package generic.lanceurSimultane;

import java.util.Scanner;

import generic.analyseurOptions.AnalyseurOptions;
import generic.analyseurOptions.Options;
import generic.jeu.JeuSimultane;
import generic.jeu.Joueur;
import generic.log.LogSingleton;

/**
 * lancement du jeu
 */
class LancerJeu {

	/**
	 * le jeu a lancer
	 */
	JeuSimultane jeu;

	/**
	 * lance mainAbstract avec les bonnes options
	 */
	public LancerJeu(String[] args, FactoryJeu factory) {
		// initialise les options
		factory.initialiserOptions();

		// analyse les options (via analyseur) et mise a jour des options
		AnalyseurOptions analyse = new AnalyseurOptions(Options.getOptions());
		analyse.analyser(args);

		// on construit le jeu souhaite
		jeu = factory.getJeu();
	}

	/**
	 * on fait tourner le jeu tant qu'il n'est pas fini
	 */
	public void boucleJeu() {

		// tant que le jeu n'est pas fini
		while (!this.jeu.etreFini()) {

			int nbJoueurs = this.jeu.getNb();

			// on demande a chaque joueur son action
			for (int i = 0; i < nbJoueurs; i++) {
				demanderJoueur(i);
			}

			// on execute le jeu
			this.jeu.executerJeu();

			// on affiche le jeu
			System.out.println(this.jeu.getStatut(0));

		}

		
		// fin du jeu
		LogSingleton.ecrire("***************fin du jeu *******************");
		LogSingleton.ecrire("Score:");
		double[] scores = this.jeu.getScore();
		for (int i = 0; i < this.jeu.getNb(); i++) {
			Joueur j = this.jeu.getJoueurs().get(i);
			// TODO gerer les noms des joueurs
			LogSingleton.ecrire("equipe " + i + "(" + j.getNom() + ") : " + scores[i]);
		}

	}

	/**
	 * on demande l'action du joueur i
	 * 
	 * @param sc
	 *            le scanner
	 * @param i
	 *            le num du joueur
	 */
	protected void demanderJoueur(int i) {
		// scanner pour lire clavier
		Scanner sc = new Scanner(System.in);

		// demande au clavier
		System.out.println("entree commande joueur " + i);
		String commande = sc.nextLine();
		// attention ne pas fermer car cela ferme le clavier

		this.jeu.determinerCommande(commande, i);
	}
}