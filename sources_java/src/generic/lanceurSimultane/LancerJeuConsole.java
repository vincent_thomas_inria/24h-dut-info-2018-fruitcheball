package generic.lanceurSimultane;

/**
 * permet de lancer un jeu en mode console
 */
public class LancerJeuConsole extends LancerJeu {

	/**
	 * permet de creer le lanceur de jeu
	 * 
	 * @param j
	 *            jeu a lancer
	 */
	public LancerJeuConsole(String[] args, FactoryJeu factory) {
		super(args, factory);
	}

}
