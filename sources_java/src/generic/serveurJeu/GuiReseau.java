package generic.serveurJeu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * classe permettant d'afficher les informations de la connection reseau
 */
@SuppressWarnings("serial")
public class GuiReseau extends JPanel {

	private static final int ESPACE = 20;
	/**
	 * taille de la fenetre d'attente
	 */
	private static final int DIM_X = 400;
	private static final int DIM_Y = 200;

	/**
	 * la frame contenant le JPanel this
	 */
	JFrame frame;

	/**
	 * nombre de joueurs attendus et actuel
	 */
	int nbTotal, nbActuel;

	/**
	 * les joueurs entres
	 */
	String[] noms;

	/**
	 * creer la fenetre
	 */
	public GuiReseau(int total) {
		// construit la fenetre
		this.setPreferredSize(new Dimension(DIM_X, DIM_Y));
		this.noms = new String[total];
		this.nbTotal = total;

		// lance la frame
		this.frame = new JFrame("Serveur - en attente");
		this.frame.setContentPane(this);
		this.frame.pack();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setVisible(true);
	}

	/**
	 * affiche le JPanel
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.black);
		g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
		g.drawRect(1, 1, this.getWidth() - 2, this.getHeight() - 2);

		g.setFont(new Font("Courier", Font.BOLD, 16));
		g.drawString(
				"Serveur en attente " + this.nbActuel + "/" + this.nbTotal,
				ESPACE, ESPACE);
		// pour chaque joueur
		for (int i = 0; i < this.nbTotal; i++) {
			if (i < nbActuel) {
				g.setColor(Color.blue);
				g.drawString("  * Joueur " + i + " ->" + noms[i], ESPACE,
						ESPACE * (i + 2));
			} else {
				g.setColor(Color.black);
				g.drawString("  * Joueur " + i + " ->", ESPACE, ESPACE
						* (i + 2));
			}
		}

		// barre de progression
		g.setColor(Color.red);
		g.fillRect(ESPACE, ESPACE * (this.nbTotal + 2),
				((DIM_X - 2 * ESPACE) * this.nbActuel) / this.nbTotal, 50);
		g.setColor(Color.black);
		g.drawRect(ESPACE, ESPACE * (this.nbTotal + 2), DIM_X - 2 * ESPACE, 50);

	}

	/**
	 * recoit un utilisateur
	 * 
	 * @param nom
	 *            de l'utilisateur connecte
	 */
	public void recevoirUser(String nom) {
		this.noms[nbActuel] = nom;
		this.nbActuel++;
		repaint();
	}

	/**
	 * on ferme la fenetre
	 */
	public void fermer() {
		this.frame.setVisible(false);
	}

}
