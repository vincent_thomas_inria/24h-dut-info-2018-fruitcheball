package generic.serveurJeu;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import generic.analyseurOptions.Options;
import generic.log.LogSingleton;

/**
 * classe en charge de gerer l'aspect reseau
 * <li>ouvrir la socket en attente sur le bon port
 * <li>attendre les actions utilisateurs et les transmettre au jeu
 */
public class ServeurReseau {

	/**
	 * la socketserveur
	 */
	ServerSocket socket;

	/**
	 * nombre de joueurs attendu
	 */
	int nbJoueursAttendus;

	/**
	 * les clients connectes au serveur
	 */
	private ClientConnecte clients[];

	/**
	 * JFrame d'affichage
	 */
	GuiReseau afficheAttente;

	/**
	 * creation d'un serveur
	 * 
	 * @param nbJ
	 *            nombre de joueurs attendus a la partie
	 */
	public ServeurReseau(int nbJ) {
		// si un soucis dans nb joueur
		if (nbJ <= 0)
			throw new Error("probleme nombre de joueurs " + nbJ);

		// nombre de joueurs attedus
		this.nbJoueursAttendus = nbJ;
		// creation du tableau des clients connectes
		this.clients = new ClientConnecte[this.nbJoueursAttendus];
	}

	/**
	 * creation de la socket
	 */
	public void lancerServeur() {
		// creation de la fenetre d'attente si pas rendu
		if (Options.getOptions().getOption("rendu").getValueInt() == 1)
			this.afficheAttente = new GuiReseau(this.nbJoueursAttendus);
		else
			this.afficheAttente = null;

		// Creation de la socket
		try {
			int port = Options.getOptions().getVal("port");
			this.socket = new ServerSocket(port);
			LogSingleton.ecrire("Serveur en attente de connexion de " + this.nbJoueursAttendus + " joueurs");
			LogSingleton.ecrire("Sur le port " + Options.getOptions().getVal("port"));
		} catch (IOException e) {
			LogSingleton.ecrire("Impossible de creer la socket sur le port " + Options.getOptions().getVal("port"));
			System.exit(0);
		}

		// attente des connections des joueurs
		int nbConnecte = 0;
		// tant qu'on n'a pas le nombre de joueurs
		while (nbConnecte != this.nbJoueursAttendus) {

			// on recupere la connection en entree
			this.connectionClient(nbConnecte);
			if (this.afficheAttente != null)
				this.afficheAttente.recevoirUser("joueur" + nbConnecte + " - " + this.clients[nbConnecte].nom);

			// gestion de l'arrivee du prochain joueur
			nbConnecte++;
		}

		// attente avant de fermer la fenetre
		attendre();
		if (this.afficheAttente != null)
			this.afficheAttente.fermer();

		// verifie qu'on a bien les connections de tout le monde
		LogSingleton.ecrire("Il y a " + nbConnecte + " joueurs connectes");
		if (nbConnecte == this.nbJoueursAttendus)
			LogSingleton.ecrire("Tout le monde est la");
		else {
			LogSingleton.ecrire("Il manque des joueurs");
			System.exit(0);
		}
	}

	private void attendre() {
		// attend 1 seconde
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * on creer la connection client
	 * 
	 * @param nbConnecte
	 *            le nombe de client deja connectes
	 */
	private void connectionClient(int nbConnecte) {
		try {
			// recuperation de la connection
			Socket socketClient = this.socket.accept();

			// creation du client
			ClientConnecte clientConnecte = new ClientConnecte(socketClient, nbConnecte);
			this.clients[nbConnecte] = clientConnecte;

			LogSingleton.ecrire("connection " + clientConnecte);
		}

		// en cas de soucis
		catch (IOException e) {
			LogSingleton
					.ecrire("Impossible d'accepter une connection sur le port " + Options.getOptions().getVal("port"));
			System.exit(0);
		}
	}

	/**
	 * fermeture du serveur
	 */
	public void close() {
		// fermeture du serveur
		LogSingleton.ecrire("fermeture du serveur");

		// on ferme tous les flux
		for (ClientConnecte client : this.clients)
			client.close();

		// on ferme flux serveur
		try {
			this.socket.close();
		} catch (IOException e) {
			LogSingleton.ecrire("probleme fermeture du flux du serveur");
		}

		// fin
		LogSingleton.ecrire("serveur ferme");
	}

	/**
	 * permet de demander au client i sa commande
	 * 
	 * @param i
	 *            le num du client
	 * @return la commande du client
	 * 
	 * @throws IOException
	 *             en cas de non reponse du client
	 */
	public String demanderClient(int i) throws IOException {
		return (this.clients[i].recevoirMessage());
	}

	/**
	 * permet d'envoyer un message au client i
	 * 
	 * @param i
	 *            le num du client
	 * @param descriptif
	 *            le message a transmettre
	 * @throws IOException
	 *             si le client ne peut pas recevoir
	 * 
	 */
	public void envoyerClient(int i, String descriptif) throws IOException {
		this.clients[i].envoyerMessage(descriptif);
	}

	/**
	 * @return les noms des clients connectes
	 */
	public String[] getNomsClients() {
		String[] noms = new String[this.clients.length];
		for (int i = 0; i < this.clients.length; i++)
			noms[i] = this.clients[i].nom;
		return noms;
	}



}
