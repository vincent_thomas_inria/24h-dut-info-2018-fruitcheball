package generic.log;

/**
 * une interface qui definit un loggeur <li>peut ecrire
 * 
 * @author vthomas
 * 
 */
public interface Log {

	/**
	 * ecrire dans les log la chaine s
	 * 
	 * @param s
	 *            chaine a ecrire
	 */
	public void ecrireLog(String s);

}
