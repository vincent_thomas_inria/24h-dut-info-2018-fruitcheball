package generic.log;

/**
 * permet de logguer sur l'ecran
 * 
 * @author vthomas
 * 
 */
public class LogEcran implements Log {

	@Override
	public void ecrireLog(String s) {
		// on ecrit dans la console
		System.out.println(s);
	}

}
