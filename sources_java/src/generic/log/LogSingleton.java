package generic.log;

import generic.analyseurOptions.Options;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * la classe permettant de tout logger
 * <p>
 * gere par un singleton
 */
public class LogSingleton {

	/**
	 * l'objet log qui a ete cree
	 */
	private static Log log = null;

	/**
	 * @return l'objet log statique
	 */
	public static Log getLog() {
		// si log n'existe pas, on le cree
		if (log == null) {
			// en fonction de l'option choisie
			Options options = Options.getOptions();
			int val = options.getVal("log");
			switch (val) {
			case 0:
				log = new LogFichier(getDate());
				break;
			case 1:
				log = new LogEcran();
				break;
			case 2:
				log = new LogVide();
				break;
			default:
				log = new LogFichier(getDate());
			}
		}
		// on retourne log
		return log;
	}

	/**
	 * permet d'ecrire dans le log courant
	 * 
	 * @param s
	 *            la chaine a ecrire
	 */
	public static void ecrire(String s) {
		Log log = getLog();
		log.ecrireLog(getDate() + " : " + s);
	}

	/**
	 * permet de construire une date pour les fichiers de log
	 * 
	 * @return date construite simplement
	 */
	public static String getSimpleDate() {
		String jour = new SimpleDateFormat("dd", Locale.FRANCE)
				.format(new Date());
		String mois = new SimpleDateFormat("MM", Locale.FRANCE)
				.format(new Date());
		String annee = new SimpleDateFormat("yyyy", Locale.FRANCE)
				.format(new Date());
		String heure = new SimpleDateFormat("HH", Locale.FRANCE)
				.format(new Date());
		String minute = new SimpleDateFormat("mm", Locale.FRANCE)
				.format(new Date());
		return jour + mois + annee + "-" + heure + minute;
	}

	/**
	 * permet de construire une date avec secondes
	 * 
	 * @return date construite
	 */
	public static String getDate() {
		String jour = new SimpleDateFormat("dd", Locale.FRANCE)
				.format(new Date());
		String mois = new SimpleDateFormat("MM", Locale.FRANCE)
				.format(new Date());
		String annee = new SimpleDateFormat("yyyy", Locale.FRANCE)
				.format(new Date());
		String heure = new SimpleDateFormat("HH", Locale.FRANCE)
				.format(new Date());
		String minute = new SimpleDateFormat("mm", Locale.FRANCE)
				.format(new Date());
		String seconde = new SimpleDateFormat("ss", Locale.FRANCE)
				.format(new Date());
		return jour + "_" + mois + "_" + annee + "-" + heure + "_" + minute
				+ "_" + seconde;
	}

}
