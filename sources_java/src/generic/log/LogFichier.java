package generic.log;

import java.io.*;

/**
 * permet de mettre les logs dans un fichier
 * 
 * @author vthomas
 * 
 */
public class LogFichier implements Log {

	/**
	 * fichier dans lequel logguer
	 */
	BufferedWriter bf;

	/**
	 * construit le fichier de log
	 * 
	 * @param date
	 */
	public LogFichier(String date) {
		// creation du fichier de log
		try {
			bf = new BufferedWriter(new FileWriter(new File(date + ".log")));
		} catch (IOException e) {
			// probleme a la creation
			e.printStackTrace();
			System.out.println("*** Erreur de creation du fichier " + date
					+ " ***");
			System.exit(1);
		}
	}

	@Override
	public void ecrireLog(String s) {
		try {
			bf.write(s+"\n");
			bf.flush();
		} catch (IOException e) {
			// probleme ecrirute
			e.printStackTrace();
			System.out.println("*** Erreur d'ecriture dans fichier ***");
			System.exit(1);
		}
	}

}
