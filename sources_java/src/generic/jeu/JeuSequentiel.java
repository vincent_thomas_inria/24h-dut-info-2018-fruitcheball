package generic.jeu;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public abstract class JeuSequentiel extends Observable implements Jeu {

	/**
	 * la liste des joueurs
	 */
	private List<Joueur> joueurs;

	/**
	 * num joueur en cours
	 */
	private int numJoueurCours;

	/**
	 * construit un jeu vide avec une liste de joueurs de taille donnee
	 * 
	 * @param nbj
	 *            nombre de joueurs
	 */
	public JeuSequentiel(int nbj) {
		this.joueurs = new ArrayList<>();
		for (int i = 0; i < nbj; i++) {
			this.joueurs.add(new Joueur());
		}
	}

	/**
	 * 
	 * @return nombre de joueurs
	 */
	public int getNb() {
		return this.joueurs.size();
	}

	/**
	 * retourne la liste des joueurs
	 * 
	 * @return la liste des joueurs
	 */
	public List<Joueur> getJoueurs() {
		return joueurs;
	}

	/**
	 * permet d'executer une evolution du jeu
	 * 
	 * @param id
	 *            id du joueur
	 * @param action
	 *            action effectuee
	 */
	final public void executerJeu(int id, String action) {
		// mettre a jour les donnees
		this.evoluerDonnees(id, action);
		// on change le joueur en cours
		setNumJoueurCours(getNumJoueurCours() + 1);
		if (getNumJoueurCours() >= getNb())
			setNumJoueurCours(0);

		// mettre a jour les vues
		this.setChanged();
		this.notifyObservers();
	}

	///////////////////////////////////////////////////////////////////////
	//////////// Methodes abstraites a redefinir dans un jeu //////////////
	///////////////////////////////////////////////////////////////////////

	/**
	 * demande la mise a jour des donnees suite a l'action du joueur id_joueur
	 * 
	 * @param idJoueur
	 *            l'id du joueur qui fait l'action
	 * @param action
	 *            action emise par le joueur
	 */
	protected abstract void evoluerDonnees(int idJoueur, String action);

	public int getNumJoueurCours() {
		return numJoueurCours;
	}

	public void setNumJoueurCours(int numJoueurCours) {
		this.numJoueurCours = numJoueurCours;
	}
}
