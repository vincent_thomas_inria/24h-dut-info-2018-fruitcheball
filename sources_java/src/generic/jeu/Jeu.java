package generic.jeu;

import java.util.List;

import javax.swing.JComponent;

public interface Jeu {

	/**
	 * @return une vue graphique sur le jeu
	 */
	public abstract JComponent getVueGraphique();

	/**
	 * methode pour retourner le statut du jeu vers les joueurs
	 * 
	 * @param numJoueur
	 *            le numero du joueur concerne
	 *
	 * @return la chaine associee a la vue du joueur numJoueur
	 */
	public abstract String getStatut(int numJoueur);

	/**
	 * determine quand le jeu est fini
	 * 
	 * @return true si et seulement si le jeu est fini
	 */
	public abstract boolean etreFini();

	/**
	 * retourne le score des joueurs
	 */
	public abstract double[] getScore();

	/**
	 * 
	 * @return nombre de joueurs
	 */
	public int getNb();

	/**
	 * retourne la liste des joueurs
	 * 
	 * @return la liste des joueurs
	 */
	public List<Joueur> getJoueurs();
}
