package generic.jeu;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * permet de representer un jeu
 */
public abstract class JeuSimultane extends Observable implements Jeu {

	/**
	 * la liste des joueurs
	 */
	private List<Joueur> joueurs;

	/**
	 * construit un jeu vide avec une liste de joueurs de taille donnee
	 * 
	 * @param nbj
	 *            nombre de joueurs
	 */
	public JeuSimultane(int nbj) {
		this.joueurs = new ArrayList<>();
		for (int i = 0; i < nbj; i++) {
			this.joueurs.add(new Joueur());
		}
	}

	/**
	 * permet a un joueur de determiner son action
	 *
	 * @param commande
	 *            commande decidee par le joueur
	 *
	 * @param numJoueur
	 *            le numero du joueur
	 */
	public void determinerCommande(String commande, int numJoueur) {

		// si le parametre est mauvais
		if ((numJoueur < 0) || (numJoueur > this.joueurs.size() - 1)) {
			throw new Error("le numero de joueur n'est pas bon " + numJoueur);
		}

		// sinon, on affecte l'action au joueur si elle n'est pas nul
		this.joueurs.get(numJoueur).setAction(commande);
	}

	/**
	 * permet d'executer une evolution du jeu une fois que toutes les commandes
	 * ont ete jouees.
	 */
	final public void executerJeu() {
		// mettre a jour les donnees
		this.evoluerDonnees();

		// mettre a jour les vues
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * 
	 * @return nombre de joueurs
	 */
	public int getNb() {
		return this.joueurs.size();
	}

	/**
	 * retourne la liste des joueurs
	 * 
	 * @return la liste des joueurs
	 */
	public List<Joueur> getJoueurs() {
		return joueurs;
	}

	///////////////////////////////////////////////////////////////////////
	//////////// Methodes abstraites a redefinir dans un jeu //////////////
	///////////////////////////////////////////////////////////////////////

	/**
	 * demande la mise a jour des donnees
	 */
	protected abstract void evoluerDonnees();


}