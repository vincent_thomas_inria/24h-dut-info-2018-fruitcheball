package generic.jeu;

/**
 * determine un joueur par defaut
 */
public class Joueur {

	/**
	 * entier qui numerote les joueurs
	 */
	static int idJoueur = 0;

	/**
	 * le nom du joueur
	 */
	String nom;

	/**
	 * la commande decidee par un joueur
	 */
	protected String commande;

	/**
	 * le nom du joueur
	 * 
	 * @param nom
	 */
	public Joueur(String nom) {
		this.nom = nom;
		idJoueur++;
	}

	/**
	 * constructeur avec un nom aleatoire
	 */
	public Joueur() {
		this.nom = "_";
		switch (idJoueur) {
		case 0:
			this.nom += "Alphonse";
			break;
		case 1:
			this.nom += "Berthe";
			break;
		case 2:
			this.nom += "Clothilde";
			break;
		case 3:
			this.nom += "Darius";
			break;
		case 4:
			this.nom += "Enee";
			break;
		case 5:
			this.nom += "Florence";
			break;
		default:
			this.nom += "Unknown";
		}
		this.nom += "_";
		idJoueur++;
	}

	/**
	 * modifie la commande courante du joueur.
	 * 
	 * @param commande
	 *            la nouvelle commande
	 * 
	 */
	public void setAction(String commande) {
		this.commande = commande;
	}

	/**
	 * @return retourne la commande de l'utilisateur
	 */
	public String getCommande() {
		return this.commande;
	}

	/**
	 * @return le nom du joueur
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * modifie le nom du client
	 * 
	 * @param nom2
	 *            nouveau nom
	 */
	public void setNom(String nom2) {
		this.nom = nom2;
	}

}
