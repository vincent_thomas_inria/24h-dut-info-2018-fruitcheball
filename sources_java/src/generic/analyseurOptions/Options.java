package generic.analyseurOptions;

import java.util.Map;
import java.util.TreeMap;

import generic.log.LogSingleton;

/**
 * represente les options du jeu. A initialiser pour chaque jeu.
 * <p>
 * utilise le patron singleton.
 * 
 * @author vincent.Thomas@loria.fr
 * 
 */
public class Options {

	// //////////////////////////////// singleton

	/**
	 * l'attribut static
	 */
	private static Options OPTIONS = null;

	/**
	 * methode static pour acceder aux options
	 */
	public static Options getOptions() {
		if (OPTIONS == null)
			OPTIONS = new Options();
		return (OPTIONS);
	}

	// //////////////////////////////////

	/**
	 * les valeurs des options
	 */
	Map<String, Option> options;

	/**
	 * la liste des options par categories
	 */
	Categories categories;

	/**
	 * construit le tableau d'options
	 */
	private Options() {
		this.options = new TreeMap<>();
		this.categories = new Categories();

		// option port par defaut
		this.setOptions("Reseau", "port", 1337, "le port sur lequel lancer le serveur");

		this.setOptions("Reseau", "timeout", 3000, "le timeout du client");
		this.getOption("timeout").setHidden(true);

		this.setOptions("Reseau", "arrettimeout", 1, "disqualifie les clients apres timeout (1-oui)");
		this.getOption("arrettimeout").setHidden(true);

		// options communes
		this.setOptions("Graphique", "rendu", 1, "avoir un rendu graphique (rendu=1) ou non (rendu=0)");
		this.getOption("rendu").setHidden(true);

		this.setOptions("Graphique", "attente", 500, "temps attente entre chaque pas de temps (ms)");

		this.setOptions("Graphique", "print", 0, "permet de sauver les images (print=1) dans ss-rep images");
		this.getOption("print").setHidden(true);

		// gestion logs
		this.setOptions("Log", "log", 1, "stockage des logs de la partie : 0-fichier 1-ecran 2-vide");

		// gestion mode batch en cache
		this.setOptions("Log", "batch", 0, "savoir si serveur attend retour utilisateur a la fin");
		this.getOption("batch").setHidden(true);
	}

	/**
	 * ajoute une option entiere
	 * 
	 * @param name
	 *            nom de l'option
	 * @param valeur
	 *            valeur de l'option
	 * @param descriptif
	 *            descriptif de l'option
	 */
	public void setOptions(String name, int valeur, String descriptif) {
		if (this.options.containsKey(name))
			throw new Error("options existante");
		this.options.put(name, new OptionEntier(name, valeur, descriptif));
		LogSingleton.ecrire("initialise valeur <" + name + " -> " + valeur + ">");

		// on ajoute dans la categorie par defaut
		this.categories.ajouterOption(name, Categories.GENERALE);
	}

	/**
	 * ajoute une option entiere en precisant la categorie
	 * 
	 * @param categorie
	 *            categorie de l'option
	 * 
	 * @param name
	 *            nom de l'option
	 * @param valeur
	 *            valeur de l'option
	 * @param descriptif
	 *            descriptif de l'option
	 */
	public void setOptions(String categorie, String name, int valeur, String descriptif) {
		if (this.options.containsKey(name))
			throw new Error("options existante");
		this.options.put(name, new OptionEntier(name, valeur, descriptif));
		System.out.println("initialise valeur <" + name + " -> " + valeur + ">");

		// on ajoute dans la categorie par defaut
		this.categories.ajouterOption(name, categorie);
	}

	/**
	 * change option entiere existante
	 * 
	 * @param name
	 *            nom de l'option
	 * @param valeur
	 *            valeur de l'option
	 */
	public void changeOptions(String name, int valeur) {
		if (!this.options.containsKey(name))
			throw new Error("options inexistante");
		this.options.get(name).setValue(valeur);
		System.out.println("change valeur <" + name + " -> " + valeur + ">");
	}

	/**
	 * change l'option
	 * 
	 * @param nom
	 *            le nom de l'option
	 * @param val
	 *            la valeur de l'option
	 */
	public void changeOptions(String nom, String val) {
		if (!this.options.containsKey(nom))
			throw new Error("options inexistante");
		System.out.println("change valeur <" + nom + " -> " + val + ">");
		this.options.get(nom).setValue(val);
	}

	/**
	 * accede a la valeur de l'option
	 * 
	 * @param name
	 *            nom de l'option
	 */
	public int getVal(String name) {
		if (!this.options.containsKey(name))
			throw new Error("options inexistante <" + name + ">");
		return this.options.get(name).getValueInt();
	}

	/**
	 * accede a l' option
	 * 
	 * @param name
	 *            nom de l'option
	 */
	public Option getOption(String name) {
		if (!this.options.containsKey(name))
			throw new Error("options existante <" + name + ">");
		return this.options.get(name);
	}

	/**
	 * retourne les categories
	 * 
	 * @return
	 */
	public String afficherCategories() {
		return "" + this.categories;
	}

	/**
	 * permet d'afficher les usages des options
	 * 
	 * @param afficheHidden
	 *            si vaut true, affiche les options cachees
	 * 
	 * @return descriptif de l'analyseur
	 */
	public String getUsage(boolean afficheHidden) {
		String explication = "\n";
		explication += "Les options attendues doivent etre passees sous la forme \n";
		// explication += " '-nom' pour ajouter un flag ayant nom pour nom\n";
		explication += "   '-valeur=xx' pour donner la valeur xx a la variable valeur\n";
		explication += "\nLes options disponibles et leurs valeurs actuelles sont:\n";

		// parcourt des options existantes par categorie
		for (Categorie c : this.categories.categories) {
			if (!c.nomOptions.isEmpty()) {
				explication += "* <" + c.nom + ">\n";
				// explication
				for (String nom : c.nomOptions) {
					Option option = this.options.get(nom);
					// si elle n'est pas cachee, on ajoute son descriptif
					if (!option.hidden || afficheHidden)
						explication += "\t'-" + nom + "=" + option.getValueInt() + "' " + option.descriptif + "\n";
				}
			}

		}
		return explication;
	}

}
