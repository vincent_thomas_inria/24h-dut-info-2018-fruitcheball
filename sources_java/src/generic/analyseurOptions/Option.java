package generic.analyseurOptions;

/**
 * permet de representer une option (abstraite)
 * 
 * @author Vthomas
 *
 */
public abstract class Option {

	/**
	 * le nom de l'option
	 */
	String nom;

	/**
	 * le descriptif
	 */
	String descriptif;

	/**
	 * option cachee ou non
	 */
	boolean hidden;

	/**
	 * creer une option
	 */
	public Option(String nom, String descriptif) {
		this.nom = nom;
		this.descriptif = descriptif;
		this.hidden = false;
	}

	/**
	 * cacher une option
	 * 
	 * @param hidden
	 *            true si on souhaite cacher option
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	/**
	 * met a jour une valeur entiere
	 * 
	 * @param valeur
	 */
	public abstract void setValue(int valeur);

	public abstract void setValue(String val);

	/**
	 * @return la valeur entiere
	 */
	public abstract int getValueInt();

}
