package generic.analyseurOptions;

import generic.log.LogSingleton;

/**
 * permet d'analyser les options et de creer l'objet options qui va bien
 * <p>
 */
public class AnalyseurOptions {

	/**
	 * les options a modifier
	 */
	Options options;

	/**
	 * construit un analyseur d'options a partir d'un ensemble d'options
	 * fournies
	 */
	public AnalyseurOptions(Options options) {
		this.options = options;

	}

	/**
	 * permet d'analyser args a partir des options connues
	 */
	public void analyser(String[] args) {
		// si c'est --help
		if (args.length == 1) {
			if (args[0].equals("--help") || args[0].equals("--h")
					|| args[0].equals("-h") || args[0].equals("-help")) {
				System.out.println(this.options.getUsage(false));
				System.exit(0);
			}

			if (args[0].equals("--helpHidden") || args[0].equals("--hidden")
					|| args[0].equals("-Hidden")) {
				System.out.println(this.options.getUsage(true));
				System.exit(0);
			}
		}

		// pour chaque argument
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];

			// si ne commence pas par "-"
			if (arg.length()<2||arg.substring(0, 2).equals("-")) {
				LogSingleton.ecrire(this.options.getUsage(false));
				LogSingleton.ecrire("**ERREUR** argument indice "+i+" '"+arg+"' ");
				throw new IllegalArgumentException("argument " + i + " <" + arg
						+ "> ne debute pas par '-option='");
			}

			// si c'ets le cas, on extrait le nom de l'argument
			arg = arg.substring(1);
			String nom = arg.split("=")[0];
			// verifie que c'est une option connue
			if (!this.options.options.containsKey(nom)) {
				LogSingleton.ecrire(this.options.getUsage(false));
				throw new IllegalArgumentException("argument " + i + " < -"
						+ arg + " > inconnu");
			}

			// on verifie qu'il existe bien une valeur
			if (arg.split("=").length != 2) {
				LogSingleton.ecrire(this.options.getUsage(false));
				throw new IllegalArgumentException("argument " + i + " < -"
						+ arg + " > ne respecte pas la forme '-nom=valeur'");
			}

			// sinon fait la modification
			options.changeOptions(nom, arg.split("=")[1]);

		}

	}

}
