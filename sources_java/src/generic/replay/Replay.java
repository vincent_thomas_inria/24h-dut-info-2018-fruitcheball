package generic.replay;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

import generic.jeu.JeuSequentiel;

/**
 * class qui fait du replay
 * 
 * @author vthomas
 * 
 */
public class Replay {

	/**
	 * jeu suivi
	 */
	JeuSequentiel jeu;

	/**
	 * constructeur et creation de la fenetre
	 */
	public Replay(JeuSequentiel j) {
		this.jeu = j;

		JFrame frame = new JFrame();
		JPanel fond = new JPanel();
		frame.setContentPane(fond);

		// ajout image
		fond.setLayout(new BorderLayout());
		fond.add(this.jeu.getVueGraphique(), BorderLayout.CENTER);

		// ajout slider et bouton
		JSlider slide = new JSlider();
		fond.add(slide, BorderLayout.SOUTH);

		// ouverture frame
		frame.pack();
		frame.setVisible(true);

	}

}
