package jeux.labyrintheSequentiel.iaRandom;

import client.random.StructureRandom;

/**
 * client par defaut random qui choisit au hasard parmi les actions donnees
 * 
 * @author vthomas
 * 
 */
public class ClientRandom {

	/**
	 * methode principale
	 * 
	 * @param args
	 *            adresse IP
	 */
	public static void main(String[] args) {
		String[] tab = { "N", "S", "E", "O" };
		new StructureRandom(args, tab);
	}

}
