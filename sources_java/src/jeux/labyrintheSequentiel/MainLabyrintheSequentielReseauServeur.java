package jeux.labyrintheSequentiel;

import generic.lanceurSequentiel.LancerJeuSeqReseau;
import jeux.labyrintheSequentiel.options.FactoryLabySeq;

public class MainLabyrintheSequentielReseauServeur {

	/**
	 * lance un serveur reseau a partir d'un jeu labyrinthe
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuSeqReseau lanceur = new LancerJeuSeqReseau(args, new FactoryLabySeq());
		lanceur.lancerJeu();
	}
}
