package jeux.labyrintheSequentiel.jeu;


public class Personnage {

	/**
	 * position du joueur
	 */
	private int x;
	private int y;

	/**
	 * constructeur par defaut
	 */
	public Personnage(int dx, int dy) {
		this.x = dx;
		this.y = dy;
	}

	/**
	 * execute le deplacement en fonction de la commande stockee
	 * 
	 * @param jeuLaby
	 */
	public void seDeplacer(String commande, int tx, int ty) {
		switch (commande) {

		// deplacement nord
		case "N":
			this.y--;
			if (this.y < 0)
				this.y = 0;
			break;

		// deplacement sud
		case "S":
			this.y++;
			if (this.y > ty - 1)
				this.y = ty - 1;
			break;

		// deplacement est
		case "E":
			this.x++;
			if (this.x > tx - 1)
				this.x = tx - 1;
			break;

		// deplacement ouest
		case "O":
			this.x--;
			if (this.x < 0)
				this.x = 0;
			break;

		// si aucune action connue
		default:
			// pas de commande
			break;
		}
	}

	/**
	 * affiche les donnees du joueurs
	 */
	@Override
	public String toString() {
		return "" + this.x + "," + this.y;
	}

	////////////////////////////////////////////////////
	/////////// getter /////////////////////////////////
	////////////////////////////////////////////////////

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
