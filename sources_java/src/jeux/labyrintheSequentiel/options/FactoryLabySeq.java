package jeux.labyrintheSequentiel.options;

import generic.analyseurOptions.Options;
import generic.jeu.JeuSequentiel;
import generic.jeu.JeuSimultane;
import generic.lanceurSequentiel.FactoryJeuSeq;
import jeux.labyrintheSequentiel.jeu.JeuLabySeq;

public class FactoryLabySeq extends FactoryJeuSeq {

	@Override
	/**
	 * initialise les options possibles du jeu
	 */
	public void initialiserOptions() {
		Options opt = Options.getOptions();

		// nombre de joueurs
		opt.setOptions("nbJoueurs", 2, "nombre de joueurs");
	}

	@Override
	/**
	 * construit un jeu avec les options
	 */
	public JeuSequentiel getJeu() {
		// recupere le nombre de joueurs passes en ligne de commande (ou par defaut)
		int val = Options.getOptions().getVal("nbJoueurs");
		// retourne le jeu
		return new JeuLabySeq(val);
	}

}
