package jeux.labyrintheSequentiel;

import generic.lanceurSequentiel.LancerJeuSeqGraphique;
import jeux.labyrintheSequentiel.options.FactoryLabySeq;

/**
 * permet de lancer le jeu labyrinthe en mode graphique
 */
public class MainLabyGraphique {

	/**
	 * lance le jeu en mode graphique
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuSeqGraphique lanceur = new LancerJeuSeqGraphique(args, new FactoryLabySeq());
		lanceur.bouclerJeu();
	}
}
