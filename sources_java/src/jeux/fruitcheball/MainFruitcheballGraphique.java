package jeux.fruitcheball;

import generic.lanceurSequentiel.LancerJeuSeqGraphique;
import jeux.fruitcheball.options.FactoryFruit;

/**
 * permet de lancer le jeu en graphique
 */
public class MainFruitcheballGraphique {

	/**
	 * creer un lanceur et l'execute
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		FactoryFruit factoryFruit = new FactoryFruit();
		LancerJeuSeqGraphique lanceur = new LancerJeuSeqGraphique(args,
				factoryFruit);
		lanceur.bouclerJeu();
	}

}
