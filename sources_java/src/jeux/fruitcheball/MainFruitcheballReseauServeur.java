package jeux.fruitcheball;

import generic.lanceurSequentiel.LancerJeuSeqReseau;
import jeux.fruitcheball.options.FactoryFruit;

public class MainFruitcheballReseauServeur {

	/**
	 * lance un serveur reseau a partir d'un jeu labyrinthe
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuSeqReseau lanceur = new LancerJeuSeqReseau(args, new FactoryFruit());
		lanceur.lancerJeu();
	}
}
