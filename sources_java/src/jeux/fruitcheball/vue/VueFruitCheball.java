package jeux.fruitcheball.vue;

import generic.analyseurOptions.Options;
import generic.log.LogSingleton;
import generic.outilVue.GestionSprites;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import jeux.fruitcheball.jeu.*;

/**
 * la vue du fruitcheball
 * 
 * @author vthomas
 * 
 */
@SuppressWarnings("serial")
public class VueFruitCheball extends JPanel implements Observer {

	/**
	 * la taille des cases
	 */
	static final int TAILLE = Options.getOptions().getVal("taille");

	/**
	 * le jeu en cours
	 */
	JeuFruitcheball jeu;

	/**
	 * gestionaire de sprites
	 */
	GestionSprites sprites;

	/**
	 * la vue du fond du labyrinthe
	 */
	VueFondLabyrinthe vueLaby;

	/**
	 * construction d'une vue de FruitCheball
	 * 
	 * @param jeuFruitcheball
	 *            le jeu de fruit a suivre
	 */
	public VueFruitCheball(JeuFruitcheball jeuFruitcheball) {
		this.jeu = jeuFruitcheball;

		// dimension de la vue
		LabyrintheFruit l = this.jeu.getLaby();
		int dimX = (l.getTailleX() + 10) * TAILLE;
		int dimY = l.getTailleY() * TAILLE;

		// si dimy n'inclut pas les joueurs
		if (dimY < 3 * jeu.getNb() * TAILLE + 4 * TAILLE) {
			dimY = (3 * jeu.getNb() * TAILLE) + 4 * TAILLE;
		}

		// mise a jour du panel
		Dimension size = new Dimension(dimX, dimY);
		this.setPreferredSize(size);
		this.setSize(size);

		// charge fruits
		LogSingleton.ecrire("chargement images");
		this.sprites = GestionSprites.getSprites();
		this.sprites.setRepertoire("images/fruitcheball");

		// creation image statique de fond
		this.vueLaby = new VueFondLabyrinthe(l);
	}

	/**
	 * repaint le composant
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		// affiche le labyrinthe
		BufferedImage fond = this.vueLaby.getFond();
		g.drawImage(fond, 0, 0, null);

		// Dessin panneau gauche
		DessinScore score = new DessinScore(this.jeu);
		score.dessinerPanneau(g);

		// affiche les personnages
		dessinerPersonnages(g2d);

		// dessin des fruits
		LabyrintheFruit laby = this.jeu.getLaby();
		for (int i = 0; i < laby.getTailleX(); i++) {
			for (int j = 0; j < laby.getTailleY(); j++) {
				Fruit f = laby.getFruits(i, j);
				if (f != null) {
					// nom du fruit
					String nom = f.getType();
					this.sprites.dessinerSprite("fruits/" + nom, i * TAILLE, j
							* TAILLE, TAILLE, g);
				}
			}
		}

	}

	/**
	 * permet de dessiner les personnages
	 * 
	 * @param g
	 *            le graphics dans lequel dessiner
	 */
	private void dessinerPersonnages(Graphics2D g) {
		DessinMeeple meeple = new DessinMeeple();
		for (int nEquipe = 0; nEquipe < this.jeu.getNb(); nEquipe++) {
			Equipe eq = this.jeu.getEquipe(nEquipe);
			// dessine personnage
			for (int numPj = 0; numPj < LabyrintheFruit.NB_PJS; numPj++) {
				Personnage p = eq.getPj(numPj);

				// dessine son action
				LastAction laction = p.getLastAction();
				if (laction != null)
					laction.dessinerAction(g, TAILLE);

				// Dessine le personnage dessus
				Position pos = p.getPos();
				int x = pos.x * TAILLE;
				int y = pos.y * TAILLE;
				meeple.dessinerMeeple(p, x, y, TAILLE, g);

			}
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		repaint();
	}

}
