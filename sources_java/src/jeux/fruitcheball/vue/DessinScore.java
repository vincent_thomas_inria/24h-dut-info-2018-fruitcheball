package jeux.fruitcheball.vue;

import generic.outilVue.Couleurs;
import generic.outilVue.GestionSprites;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Map;

import jeux.fruitcheball.jeu.Equipe;
import jeux.fruitcheball.jeu.Fruit;
import jeux.fruitcheball.jeu.JeuFruitcheball;
import jeux.fruitcheball.jeu.LabyrintheFruit;
import jeux.fruitcheball.jeu.Personnage;
import jeux.fruitcheball.jeu.Score;

public class DessinScore {

	/**
	 * taille
	 */
	public static int TAILLE = VueFruitCheball.TAILLE;

	/**
	 * les donnees
	 */
	JeuFruitcheball jeu;

	/**
	 * les sprites
	 */
	GestionSprites sprites;

	/**
	 * constructeur vide
	 */
	public DessinScore(JeuFruitcheball j) {
		this.jeu = j;
		this.sprites = GestionSprites.getSprites();

	}

	public void dessinerPanneau(Graphics g) {
		int tx = this.jeu.getLaby().getTailleX();

		// dessin du titre
		this.sprites.dessinerSprite("title", (tx) * TAILLE + TAILLE / 2,
				TAILLE / 2, 8 * TAILLE, 2 * TAILLE, g);

		// dessin nb iteration
		Graphics2D g2 = (Graphics2D) g;
		g.setColor(Color.black);
		g2.setFont(new Font("Courrier", Font.PLAIN, TAILLE / 2));
		int it = this.jeu.nbIt;
		int total = JeuFruitcheball.NB_ITERATIONS * this.jeu.getNb();

		g2.drawString("Iteration : " + it + " / " + total, (tx + 3) * TAILLE,
				3 * TAILLE);

		// dessin du score pour chaque joueur
		for (int i = 0; i < this.jeu.getNb(); i++) {
			int ox = (tx + 1) * TAILLE;
			int oy = (3 * i * TAILLE) + 3 * TAILLE;
			score(g, ox, oy, i);
		}

		// cadre autour du joueur en cours
		int tailleCadreX = 9 * TAILLE;
		int tailleCadreY = 2 * TAILLE + TAILLE/2;
		int num = this.jeu.getNumJoueurCours();
		int cx = (tx + 1) * TAILLE - TAILLE / 2;
		int cy = (3 * num * TAILLE) + 3 * TAILLE + TAILLE / 2;
		g.setColor(Couleurs.getCouleur(num));
		g.drawRect(cx, cy, tailleCadreX, tailleCadreY);
		g.drawRect(cx + 2, cy + 2, tailleCadreX - 4, tailleCadreY - 4);
		g.drawRect(cx + 3, cy + 3, tailleCadreX - 6, tailleCadreY - 6);
		g.setColor(Color.BLACK);
		g.drawRect(cx + 1, cy + 1, tailleCadreX - 2, tailleCadreY - 2);

	}

	/**
	 * permet de dessiner le score
	 * 
	 * @param g
	 *            graphics dans lequel dessiner
	 * @param ox
	 *            offset en X sur le graphics
	 * @param oy
	 *            offset en Y sur le graphics
	 * @param num
	 *            numero du joueur
	 */
	private void score(Graphics g, int ox, int oy, int num) {
		Graphics2D g2 = (Graphics2D) g;

		// dessin nom
		g2.setFont(new Font("monospaced", Font.PLAIN, TAILLE / 2));
		int y = oy + TAILLE;
		g.drawString(this.jeu.getJoueurs().get(num).getNom(), ox, y);
		oy = oy + TAILLE / 4;

		// dessin de carre pour les personnages
		Equipe equipe = this.jeu.getEquipe(num);
		DessinMeeple meeple = new DessinMeeple();
		for (int i = 0; i < LabyrintheFruit.NB_PJS; i++) {
			Personnage p = equipe.getPj(i);
			int x = ox + TAILLE * i;
			meeple.dessinerMeeple(p, x, y, TAILLE, g2);
			// dessin inventaire
			Fruit fruitPoss = p.getFruitPoss();
			if (fruitPoss != null) {
				sprites.dessinerSprite(fruitPoss.getNomImage(), x, y + TAILLE,
						TAILLE, g2);
			}
		}

		// Dessin des fruits manges
		Score score = equipe.getScore();
		int x = ox + TAILLE * 3;
		int numFruit = 0;
		Map<Fruit, Integer> fruits = score.fruits;
		for (Fruit f : fruits.keySet()) {
			int dessinX = x + (TAILLE * (numFruit + 1) / 2);
			sprites.dessinerSprite(f.getNomImage(), dessinX, y, TAILLE / 2, g2);
			numFruit++;

			// nombre
			int nb = fruits.get(f);
			g2.drawString("" + nb, dessinX + TAILLE / 8, y + TAILLE);
		}

		// score final
		int res = score.getScoreInt();
		g2.setFont(new Font("monospaced", Font.PLAIN, TAILLE));
		g2.drawString(" " + res,
				x + (TAILLE * (fruits.keySet().size() + 1)) / 2, y + TAILLE);

	}

}
