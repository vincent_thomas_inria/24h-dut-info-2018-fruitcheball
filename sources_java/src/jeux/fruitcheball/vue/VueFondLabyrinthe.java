package jeux.fruitcheball.vue;

import generic.outilVue.Couleurs;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import jeux.fruitcheball.jeu.LabyrintheFruit;
import jeux.fruitcheball.jeu.Position;

/**
 * permet de gerer la vue statique du labyrinthe (murs)
 * 
 * @author vthomas
 * 
 */
public class VueFondLabyrinthe {

	/**
	 * taille des cases
	 */
	public static int TAILLE = VueFruitCheball.TAILLE;

	/**
	 * l'image de fond generee
	 */
	BufferedImage fond;

	/**
	 * les lignes a dessiner
	 */
	List<Ligne> lignes = new ArrayList<>();

	/**
	 * creation de la vue du labyrinthe
	 * 
	 * @param laby
	 *            labyrinthe dont on dessine les murs
	 */
	public VueFondLabyrinthe(LabyrintheFruit laby) {
		this.creationFond(laby);
	}

	/**
	 * retourne image de fond
	 */
	BufferedImage getFond() {
		return (this.fond);
	}

	/**
	 * creation de l'image de fond
	 */
	private void creationFond(LabyrintheFruit laby) {
		// dimension de la vue
		int dimX = laby.getTailleX() * TAILLE;
		int dimY = laby.getTailleY() * TAILLE;

		// taille
		int tx = laby.getTailleX();
		int ty = laby.getTailleY();

		// dessin des murs horizontaux
		{
			int debutX = -1;
			for (int j = 0; j < ty; j++) {
				for (int i = 0; i < tx; i++) {
					// s'il y a un mur et que ca debute
					if (!laby.pasdeMur(new Position(i, j))) {
						if (debutX == -1)
							debutX = i;
					}
					// s'il y a pas de mur
					else {
						if (debutX != -1) {
							// on fait un trait entre debutX et ici -1
							this.stockerLigne(debutX, j, i - 1, j);
							// on revient au debut
							debutX = -1;
						}
					}
				}
				if (debutX != -1) {
					// on fait un trait entre debutX et ici -1
					this.stockerLigne(debutX, j, tx - 1, j);
					// on revient au debut
					debutX = -1;
				}
			}
		}

		// Mur verticaux
		{
			int debutY = -1;
			for (int i = 0; i < tx; i++) {
				// gere la colonne
				for (int j = 0; j < ty; j++) {
					// s'il y a un mur et que ca debute
					if (!laby.pasdeMur(new Position(i, j))) {
						if (debutY == -1)
							debutY = j;
					}
					// s'il y a pas de mur
					else {
						// debute un dessin
						if (debutY != -1) {
							// on fait un trait entre debutY et ici -1
							this.stockerLigne(i, debutY, i, j - 1);
							// on revient au debut
							debutY = -1;
						}
					}
				}
				// si fin de colonne
				if (debutY != -1) {
					// on fait un trait entre debutX et ici -1
					this.stockerLigne(i, debutY, i, ty - 1);
					debutY = -1;
				}
			}

		}

		// creation de l'image de fond
		fond = new BufferedImage(dimX, dimY, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = (Graphics2D) fond.getGraphics();
		g.setColor(Color.lightGray);
		g.fillRect(0, 0, dimX, dimY);
		
		// dessine les zones de depot
		this.dessinerDepots(laby, g);

		// dessine les cases
		g.setColor(Color.BLACK);
		for (int i = 0; i < tx; i++)
			for (int j = 0; j < ty; j++) {
				g.drawRect(i * TAILLE, j * TAILLE, TAILLE, TAILLE);
			}

		// trace les murs du labyrinthes
		this.dessinerLignes(g);

		g.dispose();
	}

	private void dessinerDepots(LabyrintheFruit laby, Graphics2D g) {
		// pour chaque depot
		for (int i = 0; i < laby.getDepots().length; i++) {
			g.setColor(Couleurs.getCouleur(i));
			// pour chaque case du depot
			Set<Position> set = laby.getDepots()[i].getSet();
			for (Position p : set) {
				// dessiner dans la couleur
				g.fillRect(p.x * TAILLE, p.y * TAILLE, TAILLE, TAILLE);
			}
		}
	}

	private void stockerLigne(int i1, int j1, int i2, int j2) {
		this.lignes.add(new Ligne(i1, i2, j1, j2));
	}

	private void dessinerLignes(Graphics2D g) {
		g.setColor(Color.darkGray);
		g.setStroke(new BasicStroke(TAILLE, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_BEVEL));

		for (Ligne l : this.lignes) {
			g.drawLine(l.i1 * TAILLE + TAILLE / 2, l.j1 * TAILLE + TAILLE / 2,
					l.i2 * TAILLE + TAILLE / 2, l.j2 * TAILLE + TAILLE / 2);
		}

		int t = TAILLE / 2;
		Color darkGreen = new Color(105, 188, 127);
		g.setColor(darkGreen);
		g.setStroke(new BasicStroke(t, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_BEVEL));
		for (Ligne l : this.lignes) {
			g.drawLine(l.i1 * TAILLE + TAILLE / 2, l.j1 * TAILLE + TAILLE / 2,
					l.i2 * TAILLE + TAILLE / 2, l.j2 * TAILLE + TAILLE / 2);
		}
	}
}
