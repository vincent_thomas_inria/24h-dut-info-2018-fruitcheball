package jeux.fruitcheball.jeu;

/**
 * un personnage du jeu de fruitcheball
 * 
 * @author vthomas
 * 
 */
public class Personnage {

	/**
	 * la distance des lancer
	 */
	private static final int DISTANCE_MAX = 5;

	/**
	 * les types de personnage
	 */
	public static int PERSO_CHEF = 0;
	public static int PERSO_NORMAL = 1;

	/**
	 * la position du personnage
	 */
	Position pos;

	/**
	 * id de l'equipe
	 */
	private int idEquipe;

	/**
	 * type de personnage
	 */
	private int type;

	/**
	 * id du personnage dans l'equipe
	 */
	private int idPj;

	/**
	 * dernier action effectuee
	 */
	private LastAction lastAction;

	/**
	 * Fruits possede
	 */
	private Fruit fruitPoss;

	/**
	 * construction de perso
	 * 
	 * @param idEq
	 *            id equipe
	 * @param i
	 *            id perso
	 */
	public Personnage(int idEq, int i) {
		// construit le personnage
		this.setIdPj(i);
		this.setIdEquipe(idEq);
		this.setFruitPoss(null);

		// si c'est le numero 0, c'est un chef
		if (i == 0)
			this.type = PERSO_CHEF;
		else
			this.type = PERSO_NORMAL;
	}

	/**
	 * change la position du personnage
	 * 
	 * @param posDepart
	 *            nouvelle position
	 */
	public void setPosition(Position posDepart) {
		this.pos = new Position(posDepart);
	}

	/**
	 * execution d'une action
	 * 
	 * @param action
	 *            action effectuee
	 * @param jeu
	 *            jeu en cours
	 */
	public void executerAction(String action, JeuFruitcheball jeu) {
		// supprime derniere action
		// TODO attention, ne fonctionne pas quand pas d'action
		this.lastAction = null;

		// aguille en fonction de l'action
		switch (action) {

		// deplacement
		case "N":
		case "S":
		case "E":
		case "O":
			this.deplacer(action, jeu);
			break;

		// prise/Echange
		case "P":
			this.echangerFruit(jeu);
			break;

		// lancer dans les directions
		case "LE":
		case "LO":
		case "LS":
		case "LN":
			this.lancerFruit(action, jeu);
			break;
		}
	}

	/**
	 * permet de lancer le fruit possede dans la direction donnee
	 * 
	 * @param action
	 *            une action de lancer
	 * @param jeu
	 *            jeu en cours
	 */
	private void lancerFruit(String action, JeuFruitcheball jeu) {
		// si pas de fruit en main, rien ne se passe
		if (this.getFruitPoss() == null)
			return;

		// lance le fruit
		Fruit fruitLance = this.getFruitPoss();
		this.setFruitPoss(null);

		// recupere la direction
		Position dir = Position.getDirection("" + action.charAt(1));

		// cherche premier obstacle jusque distance ou un intercepteur
		boolean trouve = false;
		int dist = 1;
		Position courante = new Position(this.pos);
		// la personnage qui intercepte
		Personnage intercepte = null;

		// avance dans direction
		LabyrintheFruit laby = jeu.getLaby();
		while (!trouve) {
			// decale dans la direction
			courante = courante.ajouterDep(dir);

			// si c'est un mur
			if (!laby.pasdeMur(courante)) {
				// trouve
				trouve = true;
			}
			// si c'est une personne
			else if (jeu.getPersonnage(courante) != null) {
				// trouve
				intercepte = jeu.getPersonnage(courante);
				trouve = true;
			}
			// sinon on avance d'un cran sauf si en fin
			else {
				dist++;
				// si la distance est egale distance max, trouve
				if (dist > DISTANCE_MAX)
					trouve = true;
			}
		}

		// si une personne, intercepte
		if (intercepte != null) {

			// si le fruit intercepte est dangereux et personnage normal
			if (fruitLance.danger && intercepte.type == PERSO_NORMAL) {
				// => degats et pose le fruit en main et points en moins ?
				// chataigne disparait
				intercepte.recevoirDegats(jeu, fruitLance);
				// arrete
				return;
			}

			// sinon prend le fruit si pas de fruit et arrete
			else if (intercepte.getFruitPoss() == null) {
				intercepte.setFruitPoss(fruitLance);
				this.lastAction = new LastActionLancer(this.pos, intercepte.pos);
				return;
			}
			// sinon fruit tombe si case sans fruit et arrete
			else if (laby.getFruits(courante.x, courante.y) == null) {
				laby.poserFruit(courante.x, courante.y, fruitLance);
				this.lastAction = new LastActionLancer(this.pos, courante);
				return;
			}
			// sinon le fruit rebondit sur les cases
			else {
				faireRebondirFruit(fruitLance, courante, laby);
				return;
			}
		}

		// tombe sur un mur, revient un cran en arriere
		courante = courante.ajouterDep(new Position(-dir.x, -dir.y));
		// pose le fruits sur derniere case
		if (laby.getFruits(courante.x, courante.y) == null) {
			laby.poserFruit(courante.x, courante.y, fruitLance);
			this.lastAction = new LastActionLancer(this.pos, courante);
			return;
		}

		// case occupee, le fruit rebondit
		faireRebondirFruit(fruitLance, courante, laby);
	}

	/**
	 * fait rebondir un fruit quand il doit arriver sur une case occupee
	 * 
	 * @param fruitrRebond
	 *            le fruit a faire rebondir
	 * @param courante
	 *            la position de depart
	 * @param laby
	 *            le labyrinthe
	 */
	private void faireRebondirFruit(Fruit fruitrRebond, Position courante,
			LabyrintheFruit laby) {
		// rebondit case en case au hasard jusque trouver case libre
		boolean pose = false;
		while (!pose) {
			// on deplace au hasard
			Position hasard = Position.getDirectionHasard();
			Position nouvelle = courante.ajouterDep(hasard);

			// si la case n'est pas un mur
			if (laby.pasdeMur(nouvelle)) {
				// on change la case courante
				courante = nouvelle;

				// si la case courante est vide, on pose le fruit
				if (laby.getFruits(courante.x, courante.y) == null) {
					laby.poserFruit(courante.x, courante.y, fruitrRebond);
					this.lastAction = new LastActionLancer(this.pos, courante);
					pose = true;
				}
			}
		}
	}

	/**
	 * quand un personnage reçoit des degats via un objet dangereux
	 * 
	 * @param jeu
	 *            jeu en cours
	 * @param fruitRecu
	 *            fruit (dangereux) recu par la personne
	 */
	private void recevoirDegats(JeuFruitcheball jeu, Fruit fruitRecu) {
		// fait baisser score
		Score s = jeu.getEquipe(this.idEquipe).getScore();
		s.ajouterFruit(fruitRecu);

		// fait tomber fruit
		Fruit fruit = fruitPoss;
		fruitPoss = null;

		// si la case est vide
		LabyrintheFruit laby = jeu.getLaby();
		if (laby.getFruits(this.pos.x, this.pos.y) == null) {
			// on le pose
			laby.poserFruit(this.pos.x, this.pos.y, fruit);
			return;
		}
		// sinon il rebondit
		this.faireRebondirFruit(fruit, this.pos, laby);

		//throw new Error("A faire");
	}

	/**
	 * echange le fruit possede (eventuellement null) avec la case
	 * (eventuellement null)
	 * 
	 * <p>
	 * si le personnage est normal et que le fruit est dangereux, il ne le prend
	 * pas
	 * 
	 * @param jeu
	 *            jeu en cours
	 */
	private void echangerFruit(JeuFruitcheball jeu) {
		// labyrinthe en cours
		LabyrintheFruit laby = jeu.getLaby();

		// recupere le fruit dans case courante
		Fruit fruitCase = laby.getFruits(this.pos.x, this.pos.y);

		// si le fruit est dangereux et que le personnage est normal, arret
		if (fruitCase != null && fruitCase.danger && this.type == PERSO_NORMAL) {
			// perd des points
			jeu.getEquipe(this.idEquipe).getScore().ajouterFruit(fruitCase);
			return;
		}

		// pose le fruit
		laby.poserFruit(this.pos.x, this.pos.y, this.getFruitPoss());
		// echange de fruits
		this.setFruitPoss(fruitCase);
	}

	/**
	 * permet a l'agent de se deplacer
	 * 
	 * @param action
	 *            action de deplacement
	 * @param jeu
	 *            jeu correspondant
	 */
	private void deplacer(String action, JeuFruitcheball jeu) {
		// stocke le depart
		Position depart = this.pos;

		// Calcule position arrivee
		Position direction = Position.getDirection(action);
		Position apresDep = this.pos.ajouterDep(direction);

		// tester position arrivee
		if (jeu.etreAccesibleDeplacement(apresDep))
			this.pos = apresDep;

		// stocker le deplacement
		this.lastAction = new LastActionDep(depart, this.pos);
	}

	/**
	 * permet de savoir si le personne est a la postion
	 * 
	 * @param pos2
	 * @return
	 */
	public boolean etreMemePosition(Position pos2) {
		return this.pos.equals(pos2);
	}

	// **********************************************************
	// accesseurs
	// **********************************************************

	/**
	 * @return position courante
	 */
	public Position getPos() {
		return this.pos;
	}

	public Fruit getFruitPoss() {
		return fruitPoss;
	}

	public void setFruitPoss(Fruit fruitPoss) {
		this.fruitPoss = fruitPoss;
	}

	public int getIdEquipe() {
		return idEquipe;
	}

	public void setIdEquipe(int idEquipe) {
		this.idEquipe = idEquipe;
	}

	public int getIdPj() {
		return idPj;
	}

	public void setIdPj(int idPj) {
		this.idPj = idPj;
	}

	public LastAction getLastAction() {
		return lastAction;
	}

	public void setLastAction(LastAction lastAction) {
		this.lastAction = lastAction;
	}

	/**
	 * @return le contenu du personnage
	 */
	public String toText() {
		String descr = "P";
		descr += this.idPj;
		descr += ":" + this.pos.x + ":" + this.pos.y + ":";
		if (this.fruitPoss == null)
			descr += "x";
		else
			descr += this.fruitPoss.idFruit;
		return descr;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
