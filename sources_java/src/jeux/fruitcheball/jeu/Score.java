package jeux.fruitcheball.jeu;

import java.util.Map;
import java.util.TreeMap;

/**
 * le score a un jeu de fruitcheball
 * 
 * @author vthomas
 * 
 */
public class Score {

	/**
	 * les fruits recuperes
	 */
	public Map<Fruit, Integer> fruits;

	/**
	 * constructeur vide
	 */
	public Score() {
		this.fruits = new TreeMap<>();
		// ajoute tous les fruits connus
		for (Fruit f : Fruit.FRUITS) {
			this.fruits.put(f, 0);
		}
	}

	/**
	 * @return score
	 */
	public int getScoreInt() {
		int somme = 0;
		for (Fruit f : this.fruits.keySet()) {
			// somme suite arithmetique
			int nbFruits = this.fruits.get(f);
			int points = nbFruits * (nbFruits + 1) / 2;

			// si c'est dangereux on retire les points
			if (f.danger) {
				somme = somme - points;
			} else {
				// sinon on ajoute
				somme = somme + points;
			}
		}
		return somme;
	}

	/**
	 * ajoute un fruit au score
	 * 
	 * @param fruitAAjouter
	 *            le fruit a ajouter
	 */
	public void ajouterFruit(Fruit fruitAAjouter) {
		int n = this.fruits.get(fruitAAjouter);
		this.fruits.put(fruitAAjouter, n + 1);

	}

	/**
	 * 
	 * @return descriptif du score
	 */
	public String toText() {
		//score global
		String descr = "G,";
		descr += this.getScoreInt();
		
		descr+= ",F";
		for (Fruit f : this.fruits.keySet()) {
			descr += ",F" + f.idFruit + ":" + this.fruits.get(f);
		}
		return descr;
	}

}
