package jeux.fruitcheball.jeu;

/**
 * represente une position (x,y)
 * 
 * @author vthomas
 * 
 */
public class Position {
	/**
	 * attributs publics
	 */
	public int x;
	public int y;

	/**
	 * constructeur
	 * 
	 * @param i
	 *            position en x
	 * @param j
	 *            position en y
	 */
	public Position(int i, int j) {
		this.x = i;
		this.y = j;
	}

	/**
	 * constructeur par copie
	 * 
	 * @param pos
	 *            position a copier
	 */
	public Position(Position pos) {
		this.x = pos.x;
		this.y = pos.y;
	}

	@Override
	// hashcode
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	// equals
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	/**
	 * retourne les variations de pos en fonction de la direction
	 * 
	 * @param dir
	 *            direction N, S, E, ou O
	 * @return variation de pos
	 */
	public static Position getDirection(String dir) {
		// en fonction de la direction
		switch (dir) {
		case "E":
			return new Position(1, 0);
		case "O":
			return new Position(-1, 0);
		case "N":
			return new Position(0, -1);
		case "S":
			return new Position(0, 1);
		default:
			throw new Error("direction " + dir + " inconnue");
		}
	}

	/**
	 * retourne une direction au hasard
	 * 
	 * @return direction au hasard
	 */
	public static Position getDirectionHasard() {
		String[] dirs = { "N", "E", "O", "S" };
		String direction = dirs[(int) (Math.random() * dirs.length)];
		return (getDirection(direction));
	}

	/**
	 * retourne une nouvelle pos = un deplacement + la position this
	 * 
	 * @param dep
	 *            le deplacement a ajouter
	 * @return nouvelle pos this+dep
	 */
	public Position ajouterDep(Position dep) {
		Position p = new Position(this);
		p.x = p.x + dep.x;
		p.y = p.y + dep.y;
		return p;
	}

	public String toString() {
		return "" + this.x + ":" + this.y;
	}
}
