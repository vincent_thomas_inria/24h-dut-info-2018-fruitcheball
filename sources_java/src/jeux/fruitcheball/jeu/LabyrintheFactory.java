package jeux.fruitcheball.jeu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * une factory de labyrinthe
 * 
 * @author vthomas
 * 
 */
public class LabyrintheFactory {

	/**
	 * le jeu parent du labyrinthe
	 */
	JeuFruitcheball jeu;

	/**
	 * 
	 * constructeur vide
	 */
	public LabyrintheFactory(JeuFruitcheball jeuF) {
		this.jeu = jeuF;
	}

	/**
	 * acces a un labyrinthe
	 * 
	 * @param num
	 *            numro du labyrinthe
	 */
	public LabyrintheFruit getLaby(int num) {
		switch (num) {
		case 0:
			return LabyrintheDefault();
		case 1:
			return LabyrintheDefaultV2();
		case 2:
			return LabyrintheRandom();
		case 3:
			return LabyrinthePeuFruit();
		case 4:
			return Labyrinthe4Mirabelles();
		case 5:
			return Labyrinthe4Chataignes();
		case 6:
			return LabyrinthePleinChataignes();
		case 7:
			return LabyrintheSansMur();
		case 8:
			return LabyrintheMini();
		case 9:
			return LabyrintheBig();
		case 10:
			return LabyrintheAsymetrique();

		default:
			return LabyrintheDefault();
		}

	}

	private LabyrintheFruit LabyrintheRandom() {
		// creer labyrinthe arene de taille 13
		int taille = 13;
		LabyrintheFruit laby = new LabyrintheFruit(taille, this.jeu);

		// creation mur symetrique une chance sur deux
		double res = Math.random();
		if (res < 0.3) {
			// mur laby 1
			creerMursSymetrique(3, 3, 3, 3, laby);
		} else if (res < 0.7) {
			// mur laby 2
			creerMursSymetrique(2, 3, 2, 4, laby);
			creerMursSymetrique(4, 2, 5, 2, laby);
		} else {
			// mur laby 3
			creerMursSymetrique(3, 4, 3, 5, laby);
			creerMursSymetrique(5, 3, 5, 3, laby);
		}

		// genere ordre des fruits differents pour proba differente (ca
		// ecrasement au fur et a mesure des fruits)
		List<Integer> l = new ArrayList<Integer>();
		for (int i = 0; i < 5; i++)
			l.add(i);
		Collections.shuffle(l);

		// creation des fruits (appelle 6 fois a chaque fois)
		for (int type = 0; type < 5; type++) {
			// creer 6 fruits
			for (int i = 0; i < 6; i++) {
				int espace = taille / 2 + 1;
				int dx = (int) ((Math.random() * espace)) + 1;
				int dy = (int) ((Math.random() * espace)) + 1;

				// eviter zone de depart
				if ((dx == 1) || (dx == 2))
					if (dy < 3)
						dy = 3;
				if ((dy == 1) || (dy == 2))
					if (dx < 3)
						dx = 3;

				// si c'est pas un mur
				if (laby.pasdeMur(new Position(dx, dy))) {
					// on reindexe le type avec l
					Integer typeFruit = l.get(type);
					creerFruitSymetrique(dx, dy, Fruit.FRUITS[typeFruit], laby);
				}
			}
		}

		return laby;
	}

	private LabyrintheFruit LabyrinthePleinChataignes() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(13, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(2, 3, 2, 4, laby);
		creerMursSymetrique(4, 2, 5, 2, laby);

		creerFruitSymetrique(2, 5, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(5, 5, Fruit.FRUITS[0], laby);

		creerFruitSymetrique(5, 3, Fruit.FRUITS[1], laby);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(4, 4, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(3, 6, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(3, 2, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(6, 3, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * @return un gros labyrinthe
	 */
	private LabyrintheFruit LabyrintheBig() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(18, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(3, 5, 3, 6, laby);
		creerMursSymetrique(5, 3, 6, 3, laby);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(1, 5, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(7, 7, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(5, 4, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(4, 5, Fruit.FRUITS[0], laby);

		// prunes
		creerFruitSymetrique(5, 1, Fruit.FRUITS[1], laby);
		creerFruitSymetrique(8, 6, Fruit.FRUITS[1], laby);
		creerFruitSymetrique(3, 8, Fruit.FRUITS[1], laby);

		// cerises
		creerFruitSymetrique(6, 8, Fruit.FRUITS[2], laby);
		creerFruitSymetrique(8, 3, Fruit.FRUITS[2], laby);

		// framboises
		creerFruitSymetrique(9, 9, Fruit.FRUITS[3], laby);
		creerFruitSymetrique(5, 6, Fruit.FRUITS[3], laby);
		creerFruitSymetrique(6, 5, Fruit.FRUITS[3], laby);

		// chataignes
		creerFruitSymetrique(3, 3, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(6, 6, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(7, 2, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(2, 7, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * construit un labyrinthe asymetrique
	 * 
	 * @return
	 */
	private LabyrintheFruit LabyrintheAsymetrique() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(18, 10, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(3, 5, 3, 6, laby);
		creerMursSymetrique(5, 3, 6, 3, laby);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(1, 5, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(4, 3, Fruit.FRUITS[0], laby);

		// prunes
		creerFruitSymetrique(5, 1, Fruit.FRUITS[1], laby);

		// cerises
		creerFruitSymetrique(6, 2, Fruit.FRUITS[2], laby);
		creerFruitSymetrique(8, 3, Fruit.FRUITS[2], laby);

		// framboises
		creerFruitSymetrique(6, 5, Fruit.FRUITS[3], laby);

		// chataignes
		creerFruitSymetrique(7, 2, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * construit un labyrinthe par defaut
	 * 
	 * @return labyritnhe par defaut
	 */
	private LabyrintheFruit LabyrintheDefault() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(13, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(3, 3, 3, 3, laby);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(1, 5, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(4, 4, Fruit.FRUITS[0], laby);

		// prunes
		creerFruitSymetrique(4, 1, Fruit.FRUITS[1], laby);
		creerFruitSymetrique(1, 4, Fruit.FRUITS[1], laby);
		creerFruitSymetrique(6, 6, Fruit.FRUITS[1], laby);
		creerFruitSymetrique(5, 5, Fruit.FRUITS[1], laby);

		// cerises
		creerFruitSymetrique(6, 8, Fruit.FRUITS[2], laby);
		creerFruitSymetrique(8, 3, Fruit.FRUITS[2], laby);
		creerFruitSymetrique(8, 6, Fruit.FRUITS[2], laby);
		creerFruitSymetrique(5, 1, Fruit.FRUITS[2], laby);

		// framboises
		creerFruitSymetrique(2, 6, Fruit.FRUITS[3], laby);
		creerFruitSymetrique(6, 1, Fruit.FRUITS[3], laby);
		creerFruitSymetrique(2, 3, Fruit.FRUITS[3], laby);

		// chataignes
		creerFruitSymetrique(3, 4, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(7, 3, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(2, 7, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(4, 5, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * construit un labyrinthe par defaut
	 * 
	 * @return labyritnhe par defaut
	 */
	private LabyrintheFruit LabyrintheDefaultV2() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(13, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(2, 3, 2, 4, laby);
		creerMursSymetrique(4, 2, 5, 2, laby);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(1, 5, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(5, 1, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(5, 4, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(4, 5, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(6, 6, Fruit.FRUITS[0], laby);

		// prunes
		creerFruitSymetrique(4, 1, Fruit.FRUITS[1], laby);
		creerFruitSymetrique(1, 4, Fruit.FRUITS[1], laby);

		// cerises
		creerFruitSymetrique(6, 8, Fruit.FRUITS[2], laby);
		creerFruitSymetrique(8, 3, Fruit.FRUITS[2], laby);
		creerFruitSymetrique(8, 6, Fruit.FRUITS[2], laby);

		// framboises
		creerFruitSymetrique(5, 6, Fruit.FRUITS[3], laby);
		creerFruitSymetrique(6, 5, Fruit.FRUITS[3], laby);
		creerFruitSymetrique(3, 6, Fruit.FRUITS[3], laby);

		// chataignes
		creerFruitSymetrique(3, 3, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(7, 3, Fruit.FRUITS[4], laby);
		creerFruitSymetrique(2, 7, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * construit un labyrinthe sans mur avec moins de fruits
	 * 
	 * @return labyritnhe par defaut
	 */
	private LabyrintheFruit LabyrintheSansMur() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(13, this.jeu);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(1, 5, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(5, 1, Fruit.FRUITS[0], laby);

		// prunes
		creerFruitSymetrique(4, 1, Fruit.FRUITS[1], laby);
		creerFruitSymetrique(1, 4, Fruit.FRUITS[1], laby);

		// cerises
		creerFruitSymetrique(8, 6, Fruit.FRUITS[2], laby);

		// framboises
		creerFruitSymetrique(6, 5, Fruit.FRUITS[3], laby);

		// chataignes
		creerFruitSymetrique(3, 3, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * construit un labyrinthe avec moins de fruits que defaut
	 * 
	 * @return labyritnhe par defaut
	 */
	private LabyrintheFruit LabyrinthePeuFruit() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(13, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(2, 3, 2, 4, laby);
		creerMursSymetrique(4, 2, 5, 2, laby);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(1, 5, Fruit.FRUITS[0], laby);
		creerFruitSymetrique(5, 1, Fruit.FRUITS[0], laby);

		// prunes
		creerFruitSymetrique(4, 1, Fruit.FRUITS[1], laby);
		creerFruitSymetrique(1, 4, Fruit.FRUITS[1], laby);

		// cerises
		creerFruitSymetrique(8, 6, Fruit.FRUITS[2], laby);

		// framboises
		creerFruitSymetrique(6, 5, Fruit.FRUITS[3], laby);

		// chataignes
		creerFruitSymetrique(3, 3, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * construit un labyrinthe avec 4 mirabelles
	 * 
	 * @return labyritnhe par defaut
	 */
	private LabyrintheFruit Labyrinthe4Mirabelles() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(13, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(2, 3, 2, 4, laby);
		creerMursSymetrique(4, 2, 5, 2, laby);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(4, 4, Fruit.FRUITS[0], laby);

		return laby;
	}

	/**
	 * construit un labyrinthe avec 4 chataignes
	 * 
	 * @return labyritnhe par defaut
	 */
	private LabyrintheFruit Labyrinthe4Chataignes() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(13, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(2, 3, 2, 4, laby);
		creerMursSymetrique(4, 2, 5, 2, laby);

		// creation des fruits
		// mirabelles
		creerFruitSymetrique(4, 4, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * construit un labyrinthe par defaut
	 * 
	 * @return labyritnhe par defaut
	 */
	private LabyrintheFruit LabyrintheMini() {
		// creer labyrinthe arene
		LabyrintheFruit laby = new LabyrintheFruit(9, this.jeu);

		// creation mur symetrique
		creerMursSymetrique(2, 2, 3, 2, laby);
		creerMursSymetrique(2, 2, 2, 3, laby);

		// mirabelle
		// creerFruitSymetrique(3, 3, Fruit.FRUITS[0], laby);

		// framboise
		laby.setFruits(4, 1, Fruit.FRUITS[3]);
		laby.setFruits(1, 4, Fruit.FRUITS[2]);
		laby.setFruits(4, 7, Fruit.FRUITS[1]);
		laby.setFruits(7, 4, Fruit.FRUITS[0]);

		// chataigne
		creerFruitSymetrique(4, 4, Fruit.FRUITS[4], laby);

		return laby;
	}

	/**
	 * permet de poser les fruits de manière symetrique
	 * 
	 * @param i
	 *            position x
	 * @param j
	 *            position y
	 * @param fruit
	 *            fruite depose
	 * @param laby
	 *            labyrinthe
	 */
	private void creerFruitSymetrique(int i, int j, Fruit fruit,
			LabyrintheFruit laby) {

		int tailleX = laby.getTailleX();
		int tailleY = laby.getTailleY();

		// pose les 4 memes fruits
		laby.setFruits(i, j, fruit);
		laby.setFruits(tailleX - 1 - i, j, fruit);
		laby.setFruits(i, tailleY - 1 - j, fruit);
		laby.setFruits(tailleX - 1 - i, tailleY - 1 - j, fruit);
	}

	/**
	 * creer un bloc symetrique par rapport aux deux axes dans le labyrinthe
	 * 
	 * @param xDep
	 *            x depart
	 * @param yDep
	 *            y depart
	 * @param xArr
	 *            x arrivee
	 * @param yArr
	 *            y arrivee
	 * @param l
	 *            labyrinthe
	 */
	private void creerMursSymetrique(int xDep, int yDep, int xArr, int yArr,
			LabyrintheFruit l) {
		// taille pour la symetrie
		int tailleX = l.getTailleX();
		int tailleY = l.getTailleY();

		// creation du bloc
		for (int i = xDep; i < xArr + 1; i++)
			for (int j = yDep; j < yArr + 1; j++) {
				l.murs[i][j] = true;
				l.murs[tailleX - 1 - i][j] = true;
				l.murs[i][tailleY - 1 - j] = true;
				l.murs[tailleX - 1 - i][tailleY - 1 - j] = true;
			}
	}
}
