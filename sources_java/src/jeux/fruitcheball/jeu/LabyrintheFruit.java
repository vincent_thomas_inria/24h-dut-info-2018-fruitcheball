package jeux.fruitcheball.jeu;

/**
 * le labyrinthe du jeu fruitcheball
 * 
 */
public class LabyrintheFruit {

	public static final int NB_PJS = 3;

	/**
	 * nombre max d'equipes
	 */
	public static final int NB_EQUIPES = 4;

	/**
	 * le jeu d'origine
	 */
	JeuFruitcheball jeu;

	/**
	 * les murs du labyrnthe
	 */
	boolean murs[][];

	/**
	 * les fruits du labyrinthe
	 */
	private Fruit[][] fruits;

	/**
	 * les departs des personnages
	 */
	private Position[][] departs;

	/**
	 * les zones de depot
	 */
	private ZoneDepot[] depots;

	/**
	 * creation du labyrinthe
	 * 
	 * @param taille
	 *            taille du labyrinthe
	 */
	public LabyrintheFruit(int taille, JeuFruitcheball jeuF) {
		// lien vers le jeu parent
		this.jeu = jeuF;

		// construction du labyrinthe
		if (taille < 5)
			taille = 5;
		this.murs = new boolean[taille][taille];
		this.fruits = new Fruit[taille][taille];

		// creation arene par defaut
		this.creerArene();

		// contenus (mur et fruits) a voir dans les factory

		// creation des centres des departs
		Position[] departsCentre = new Position[4];
		departsCentre[0] = new Position(1, 1);
		departsCentre[1] = new Position(taille - 2, taille - 2);
		departsCentre[2] = new Position(1, taille - 2);
		departsCentre[3] = new Position(taille - 2, 1);

		// creation des departs [nb_equipe][nb_perso]
		this.departs = new Position[NB_EQUIPES][NB_PJS];

		// creation des points de depart
		for (int eq = 0; eq < NB_EQUIPES; eq++) {
			Position dep = departsCentre[eq];
			// decalage a considerer en fonction du coin
			int dx = -1;
			if (dep.x < taille / 2) {
				dx = +1;
			}
			int dy = -1;
			if (dep.y < taille / 2) {
				dy = +1;
			}

			this.departs[eq][2] = new Position(dep.x, dep.y);
			this.departs[eq][0] = new Position(dep.x + dx, dep.y);
			this.departs[eq][1] = new Position(dep.x, dep.y + dy);
		}

		// creation des zones de depot au points de depart
		this.setDepots(new ZoneDepot[4]);
		for (int i = 0; i < 4; i++) {
			// toutes les cases de depart de l'equipe i
			ZoneDepot zoneDepot = new ZoneDepot(i);
			for (Position p : this.departs[i])
				zoneDepot.add(p);	
			this.getDepots()[i] = zoneDepot;
			
		}

	}

	/**
	 * creation du labyrinthe rectangulaire
	 * 
	 * @param taille
	 *            taille du labyrinthe
	 */
	public LabyrintheFruit(int tailleX, int tailleY, JeuFruitcheball jeuF) {
		// lien vers le jeu parent
		this.jeu = jeuF;

		// construction du labyrinthe
		if (tailleX < 5)
			tailleX = 5;
		if (tailleY < 5)
			tailleY = 5;

		this.murs = new boolean[tailleX][tailleY];
		this.fruits = new Fruit[tailleX][tailleY];

		// creation arene par defaut
		this.creerArene();

		// contenus (mur et fruits) a voir dans les factory

		// creation des centres des departs
		Position[] departsCentre = new Position[4];
		departsCentre[0] = new Position(1, 1);
		departsCentre[1] = new Position(tailleX - 2, tailleY - 2);
		departsCentre[2] = new Position(1, tailleY - 2);
		departsCentre[3] = new Position(tailleX - 2, 1);

		// creation des departs [nb_equipe][nb_perso]
		this.departs = new Position[NB_EQUIPES][NB_PJS];

		// creation des points de depart
		for (int eq = 0; eq < NB_EQUIPES; eq++) {
			Position dep = departsCentre[eq];
			this.departs[eq][0] = new Position(dep.x, dep.y);
			int dx = -1;
			if (dep.x < tailleX / 2) {
				dx = +1;
			}
			this.departs[eq][1] = new Position(dep.x + dx, dep.y);
			int dy = +1;
			if (dep.y < tailleY / 2) {
				dy = -1;
			}
			this.departs[eq][2] = new Position(dep.x, dep.y - +dy);
		}

		// creation des zones de depot au points de depart
		this.setDepots(new ZoneDepot[4]);
		for (int i = 0; i < 4; i++) {
			// toutes les cases de depart de l'equipe i
			ZoneDepot zoneDepot = new ZoneDepot(i);
			for (Position p : this.departs[i])
				zoneDepot.add(p);
			this.getDepots()[i] = zoneDepot;
		}

	}

	/**
	 * creer une arene autour du labyrinthe
	 * 
	 * @param l
	 *            labyrinthe a construire
	 */
	private void creerArene() {
		int tailleX = this.getTailleX();
		int tailleY = this.getTailleY();

		// creation de l'arene en X
		for (int i = 0; i < tailleX; i++) {
			this.murs[i][tailleY - 1] = true;
			this.murs[i][0] = true;
		}

		// sur Y
		for (int j = 0; j < tailleY; j++) {
			this.murs[0][j] = true;
			this.murs[tailleX - 1][j] = true;
		}
	}

	/**
	 * @return tailleX
	 */
	public int getTailleX() {
		return this.murs.length;
	}

	/**
	 * @return tailleY
	 */
	public int getTailleY() {
		return this.murs[0].length;
	}

	/**
	 * savoir si on est dehors
	 * 
	 * @param pos
	 *            position testee
	 * @return true si on est hors du laby
	 */
	private boolean etreDehors(Position pos) {
		return (pos.x < 0) || (pos.y < 0) || (pos.x >= this.getTailleX()) || (pos.y >= this.getTailleY());
	}

	/**
	 * les departs des personnages
	 */
	public Position getDepart(int nEquipe, int nPj) {
		if ((nEquipe < 0) || (nEquipe >= NB_EQUIPES))
			throw new Error("probleme de num d'equipes ");
		if ((nPj < 0) || (nPj >= NB_PJS))
			throw new Error("probleme de num de joueurs ");

		return this.departs[nEquipe][nPj];
	}

	/**
	 * retourne le decriptif du labyrinthe
	 * 
	 * @return
	 */
	public String toText() {
		String etat = "";

		// retourne d'abord la taille du jeu
		etat += this.getTailleX();
		etat += ":";
		etat += this.getTailleY();
		etat += ",";

		// retourne contenu du labyrinthe
		for (int j = 0; j < this.getTailleY(); j++) {
			String ligne = "";
			// ecrit la ligne
			for (int i = 0; i < this.getTailleX(); i++) {
				// si c'est un mur ==> 'X'
				if (this.murs[i][j])
					ligne += "X";
				// si c'est un fruit
				else if (this.fruits[i][j] != null) {
					ligne += this.fruits[i][j].idFruit;
				}
				// il n'y a rien
				else {
					ligne += ".";
				}
			}
			// fin de ligne
			ligne += ",";
			etat += ligne;
		}

		return etat.substring(0, etat.length() - 1);
	}

	/**
	 * verifie si la case est libre ou non
	 * <li>pas de mur
	 * 
	 * @param positionTestee
	 *            la position testee
	 * @return true si case disponible
	 */
	public boolean pasdeMur(Position positionTestee) {
		if (etreDehors(positionTestee))
			return false;
		return !this.murs[positionTestee.x][positionTestee.y];
	}

	public ZoneDepot[] getDepots() {
		return depots;
	}

	public void setDepots(ZoneDepot[] depots) {
		this.depots = depots;
	}

	/**
	 * poser le fruit dans le labyrinthe. Si c'est une zone de depot, le fait
	 * disparaitre et accumule les points.
	 * 
	 * @param x
	 *            position ou le fruit est pose
	 * @param y
	 *            position ou le fruit est pose
	 * @param fruitAPoser
	 *            le fruit a poser
	 */
	public void poserFruit(int x, int y, Fruit fruitAPoser) {

		// si un vrai fruit, chercher si marque score
		if (fruitAPoser != null) {
			// cherche si c'est une zone de depot
			Position position = new Position(x, y);
			for (ZoneDepot zone : this.depots) {
				if (zone.etreZoneDepot(position)) {
					// ajoute dans les scores
					int numEquipe = zone.idEquipe;
					this.jeu.ajouterScoreEquipe(numEquipe, fruitAPoser);
					// le fruit disparait
					fruitAPoser = null;
				}
			}
		}

		// depose dans la case
		this.fruits[x][y] = fruitAPoser;
	}

	/**
	 * retourne le fruit en (x,y) - eventuellement null si pas de fruit
	 * 
	 * @param x
	 *            position dans laby
	 * @param y
	 *            position dans laby
	 * @return fruit a la position
	 */
	public Fruit getFruits(int x, int y) {
		return this.fruits[x][y];
	}

	public void setFruits(int i, int j, Fruit fruit) {
		this.fruits[i][j] = fruit;
	}
}
