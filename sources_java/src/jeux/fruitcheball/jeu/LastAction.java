package jeux.fruitcheball.jeu;

import java.awt.Graphics2D;

/**
 * permet de stocker la derniere action faite
 * 
 * @author vthomas
 * 
 */
public interface LastAction {

	/**
	 * permet de dessiner l'action
	 * 
	 * @param g
	 *            graphics
	 * @param taille
	 *            taille des cases
	 */
	public void dessinerAction(Graphics2D g, int taille);

}
