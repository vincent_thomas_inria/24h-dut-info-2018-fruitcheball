package jeux.fruitcheball.jeu;

import java.util.ArrayList;
import java.util.List;

/**
 * represente l'equipe d'un joueur
 * 
 * @author vthomas
 * 
 */
public class Equipe {

	/**
	 * les personnages de l'equipe
	 */
	List<Personnage> persos;

	/**
	 * numero de l'equipe
	 */
	int idEquipe;

	/**
	 * Le score de l'equipe
	 */
	private Score score;

	/**
	 * creation d'une equipe
	 * 
	 * @param nb
	 *            numero de l'equipe
	 */
	public Equipe(int nb) {
		// mise a jour numero equipe
		this.idEquipe = nb;

		// score vide
		this.score = new Score();

		// construction des personnages de l'equipe
		this.persos = new ArrayList<>();

		// pour chaque personnage vide
		for (int i = 0; i < LabyrintheFruit.NB_PJS; i++) {
			this.persos.add(new Personnage(this.idEquipe, i));
		}
	}

	/**
	 * retourne le personnage de l'equipe
	 * 
	 * @param numPj
	 *            numero du personnage
	 * @return le personnage numero numPj
	 */
	public Personnage getPj(int numPj) {
		return this.persos.get(numPj);
	}

	public Score getScore() {
		return score;
	}

	public void setScore(Score score) {
		this.score = score;
	}

	/**
	 * @param zone
	 *            la zone de l'equipe
	 * @return descriptif de l'equipe
	 */
	public String toText(ZoneDepot zone) {
		String descr = "Equipe";
		// num equipe
		descr += this.idEquipe;
		descr += ",";

		// chaque joueur
		descr+="P,";
		for (int i = 0; i < this.persos.size(); i++) {
			Personnage p = this.persos.get(i);
			descr += p.toText();
			descr += ",";
		}
		
		//zones de depot
		descr+="Z,";
		descr += zone.toText();
		descr+=",";

		// points
		descr += this.score.toText();
		
		

		return descr;
	}

}
