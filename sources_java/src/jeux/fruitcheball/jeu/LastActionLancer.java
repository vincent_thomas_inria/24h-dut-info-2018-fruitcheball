package jeux.fruitcheball.jeu;

import java.awt.BasicStroke;
import java.awt.Graphics2D;

/**
 * si c'est un deplacement
 * 
 * @author vthomas
 * 
 */
public class LastActionLancer implements LastAction {

	/**
	 * le depart
	 */
	Position dep;

	/**
	 * l'arrivee tentee
	 */
	Position arr;

	/**
	 * permet de stocker l'action
	 * 
	 * @param d
	 *            depart
	 * @param a
	 *            arrivee
	 */
	public LastActionLancer(Position d, Position a) {
		this.dep = d;
		this.arr = a;
	}

	/**
	 * permet de dessiner l'action
	 * 
	 * @param g
	 *            graphics dans lequel dessiner
	 * @param taille
	 *            taille des cases
	 */
	// TODO a voir si on refactore
	public void dessinerAction(Graphics2D g, int taille) {
		BasicStroke dashed = new BasicStroke(taille/8, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_BEVEL,10.f,new float[]{10.f},0.0f);
		g.setStroke(dashed);
		// dessiner un deplacement = tracer le trait
		g.drawLine(dep.x * taille + taille / 2, dep.y * taille + taille / 2,
				arr.x * taille + taille / 2, arr.y * taille + taille / 2);
	}

}
