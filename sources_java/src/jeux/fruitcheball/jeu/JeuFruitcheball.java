package jeux.fruitcheball.jeu;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import jeux.fruitcheball.vue.VueFruitCheball;

import generic.analyseurOptions.Options;
import generic.jeu.JeuSequentiel;

/**
 * jeu de fruitcheball pour les 24h DUT de 2017-2018
 * 
 * @author vthomas
 * 
 */
public class JeuFruitcheball extends JeuSequentiel {

	/**
	 * nombre total iterations par joueur
	 */
	public static int NB_ITERATIONS = 100;

	/**
	 * une arrayList d'equipe
	 */
	List<Equipe> equipes;

	/**
	 * un labyrinthe
	 */
	LabyrintheFruit laby;

	/**
	 * nombre d'iteration
	 */
	public int nbIt = 0;

	/**
	 * construit un jeu de fruitchball avec un nombre de joueurs
	 * 
	 * @param nbj
	 *            nombre de joueurs
	 */
	public JeuFruitcheball(int nbj) {
		// on constuit les joueurs
		super(nbj);

		// on recupere le labyrinthe en fonction de l'option laby
		LabyrintheFactory labyfactory = new LabyrintheFactory(this);
		int numeroLaby = Options.getOptions().getVal("laby");
		this.laby = labyfactory.getLaby(numeroLaby);

		// on construit les equipes
		this.equipes = new ArrayList<>();
		for (int i = 0; i < nbj; i++) {
			this.equipes.add(new Equipe(i));
		}

		// on positionne tous les personnages par equipe
		for (int numEquipe = 0; numEquipe < nbj; numEquipe++) {
			Equipe equipe = this.equipes.get(numEquipe);
			// par personnage
			for (int numPj = 0; numPj < LabyrintheFruit.NB_PJS; numPj++) {
				Position posDepart = this.laby.getDepart(numEquipe, numPj);
				Personnage pj = equipe.getPj(numPj);
				pj.setPosition(posDepart);
			}
		}
	}

	/**
	 * retourne vue fruitcheball
	 */
	@Override
	public JComponent getVueGraphique() {
		VueFruitCheball vueFruitCheball = new VueFruitCheball(this);
		this.addObserver(vueFruitCheball);
		return vueFruitCheball;
	}

	@Override
	public String getStatut(int numJoueur) {
		// le descriptif de la situation de jeu
		String desc = "";
		desc = desc + numJoueur + SerialisationJeu.SEP_TEXT;
		desc += this.getNb() + SerialisationJeu.SEP_TEXT;

		// le descriptif du labyrinthe
		desc += this.laby.toText() + SerialisationJeu.SEP_TEXT;

		// les equipes
		for (int i = 0; i < this.getNb(); i++) {
			ZoneDepot zone = this.laby.getDepots()[i];
			desc += this.equipes.get(i).toText(zone)
					+ SerialisationJeu.SEP_TEXT;
		}

		// retire dernier retour chariot
		return desc.substring(0, desc.length() - 1);
	}

	@Override
	public boolean etreFini() {
		// le jeu s'arrete quand le nombre d'iterations par joueur depasse seuil
		return this.nbIt >= NB_ITERATIONS * this.getNb();
	}

	@Override
	public double[] getScore() {
		// gestion des scores
		double[] score = new double[this.getNb()];

		// pour chaque equipe
		for (int i = 0; i < this.getNb(); i++) {
			// trouve son score
			Equipe equipe = this.getEquipe(i);
			Score scoreEquipe = equipe.getScore();
			score[i] = scoreEquipe.getScoreInt();
		}

		return score;
	}

	@Override
	/**
	 * action de la forme "X-X-X" une action par personnage dans l'ordre des pjs
	 * <ul>
	 * <li>N,S,E,O deplacement
	 * <li>X rien faire
	 * <li>LN, LS, LE, LO pour lancer le fruit possede dans la direction
	 * <li>P pour prendre/poser/echanger
	 * </ul>
	 */
	protected void evoluerDonnees(int id_joueur, String action) {
		// on annule les anciennes actions de tout le monde
		Equipe enCours = this.equipes.get(id_joueur);
		for (int i = 0; i < LabyrintheFruit.NB_PJS; i++) {
			enCours.getPj(i).setLastAction(null);
		}

		// action null
		if (action == null)
			action = "";

		// construit les actions par personnage en pensant a completer
		String actions[] = { "X", "X", "X" };
		String actionsFaites[] = action.toUpperCase().split("-");
		for (int i = 0; i < actionsFaites.length && i < 3; i++) {
			actions[i] = actionsFaites[i];
		}

		// execution des actions par les personages dans l'ordre
		for (int i = 0; i < actionsFaites.length && i < 3; i++) {
			enCours.getPj(i).executerAction(actionsFaites[i], this);
		}
		nbIt++;
	}

	/**
	 * retourne personnage a la position pos
	 * 
	 * @param pos
	 *            position ou on cherche un personnage
	 * @return null si pas de perso en pos
	 */
	public Personnage getPersonnage(Position pos) {
		// parcours equipes
		for (int i = 0; i < this.equipes.size(); i++) {
			Equipe equipe = this.equipes.get(i);
			// pour chaque personnage
			for (int j = 0; j < LabyrintheFruit.NB_PJS; j++) {
				Personnage pj = equipe.getPj(j);
				if (pj.etreMemePosition(pos))
					return (pj);
			}
		}
		// pas de personnage
		return null;
	}

	/**
	 * retourne true si la case est accessible pour un personnage <li>pas de mur
	 * <li>pas d'autre pj
	 * 
	 * @param pos
	 *            position ou on cherche a aller
	 * @return true si case accessible
	 */
	public boolean etreAccesibleDeplacement(Position pos) {
		// si un mur
		if (!this.laby.pasdeMur(pos))
			return false;
		// si un personnage return false
		if (this.getPersonnage(pos) != null)
			return false;
		return true;
	}

	/**
	 * ajoute un fruit au score d'une equipe
	 * 
	 * @param numEquipe
	 *            numero equipe
	 * @param fruitAPoser
	 *            fruit a ajouter
	 */
	public void ajouterScoreEquipe(int numEquipe, Fruit fruitAPoser) {
		// verifie que numEquipe ne depasse pas
		if (numEquipe < this.equipes.size()) {
			Equipe eq = this.equipes.get(numEquipe);
			eq.getScore().ajouterFruit(fruitAPoser);
		}
	}

	// ****************************************************************
	// Accesseurs
	// ****************************************************************

	/**
	 * @return labyrinthe du jeu
	 */
	public LabyrintheFruit getLaby() {
		return this.laby;
	}

	public Equipe getEquipe(int nEquipe) {
		return this.equipes.get(nEquipe);
	}

}
