package jeux.labyrintheSimultane.jeu;

import javax.swing.JComponent;

import generic.jeu.JeuSimultane;
import generic.jeu.Joueur;
import jeux.labyrintheSimultane.vue.VueLaby;

/**
 * permet de representer un jeu de deplacement dans une arene avec plusieurs
 * joueurs
 */
public class JeuLaby extends JeuSimultane {

	/**
	 * taille du labyrinthe selon X
	 */
	private int tailleX;

	/**
	 * taille du labyrinthe selon t
	 */
	private int tailleY;

	/**
	 * le nombre d'iteration
	 */
	int nbIterations;

	/**
	 * les personnages du labyrinthe
	 */
	public Personnage[] personnages;

	/**
	 * creation du jeu par defaut
	 */
	public JeuLaby(int nbJoueurs) {
		// creation des joueurs
		super(nbJoueurs);

		// creation du labyrinthe
		this.tailleX = 10;
		this.tailleY = 10;
		this.nbIterations = 0;

		// creation des joueurs et des personnages
		// TODO erreur que deux joueurs
		this.personnages = new Personnage[nbJoueurs];
		this.personnages[0] = new Personnage(0, 0);
		this.personnages[1] = new Personnage(9, 9);
	}

	/**
	 * permet d'executer le jeu en deplacant tous les personnages
	 */
	@Override
	protected void evoluerDonnees() {
		// pour chaque joueur
		for (int i = 0; i < this.getNb(); i++) {
			// on caste en joueur laby
			Personnage pj = this.personnages[i];
			// recupere la commande
			String commande = this.getJoueurs().get(i).getCommande();
			// on execute son action
			pj.seDeplacer(commande, this.tailleX, this.tailleY);
		}

		// on augmente le temps
		this.nbIterations++;
	}

	@Override
	/**
	 * le statut du labyrinthe est simplement les positions des joueurs
	 */
	public String getStatut(int numJoueur) {
		String resultat = "";
		// on retourne simplement la suite x,y-
		for (Joueur joueur : getJoueurs()) {
			resultat += joueur.toString() + " ";
		}
		return resultat;
	}

	@Override
	/**
	 * le jeu s'arrete au bout d'un certain temps
	 */
	public boolean etreFini() {
		return (this.nbIterations > 50);
	}

	@Override
	public double[] getScore() {
		throw new Error("not implemented");
	}

	@Override
	/**
	 * retourne une vue sur le jeu
	 */
	public JComponent getVueGraphique() {
		VueLaby vue = new VueLaby(this);
		this.addObserver(vue);
		return (vue);
	}

	////////////////////////////////////////////////////
	/////////// getter /////////////////////////////////
	////////////////////////////////////////////////////

	public int getTailleX() {
		return tailleX;
	}

	public int getTailleY() {
		return tailleY;
	}

}
