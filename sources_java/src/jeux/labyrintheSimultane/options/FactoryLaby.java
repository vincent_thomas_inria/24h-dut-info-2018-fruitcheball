package jeux.labyrintheSimultane.options;

import generic.analyseurOptions.Options;
import generic.jeu.JeuSimultane;
import generic.lanceurSimultane.FactoryJeu;
import jeux.labyrintheSimultane.jeu.JeuLaby;

public class FactoryLaby extends FactoryJeu {

	@Override
	/**
	 * initialise les options possibles du jeu
	 */
	public void initialiserOptions() {
		Options opt = Options.getOptions();

		// nombre de joueurs
		opt.setOptions("nbJoueurs", 2, "nombre de joueurs");
	}

	@Override
	/**
	 * construit un jeu avec les options
	 */
	public JeuSimultane getJeu() {
		int val = Options.getOptions().getVal("nbJoueurs");
		return new JeuLaby(val);
	}

}
