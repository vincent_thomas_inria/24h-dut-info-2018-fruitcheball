package jeux.labyrintheSimultane;

import generic.lanceurSimultane.LancerJeuGraphique;
import jeux.labyrintheSimultane.options.FactoryLaby;

/**
 * permet de lancer le jeu labyrinthe en mode graphique
 */
public class MainLabyGraphique {

	/**
	 * lance le jeu en mode graphique
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuGraphique lanceur = new LancerJeuGraphique(args, new FactoryLaby());
		lanceur.boucleJeu();
	}
}
