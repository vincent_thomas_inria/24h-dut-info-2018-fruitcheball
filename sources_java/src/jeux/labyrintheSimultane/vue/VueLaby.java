package jeux.labyrintheSimultane.vue;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;

import jeux.labyrintheSimultane.jeu.JeuLaby;
import jeux.labyrintheSimultane.jeu.Personnage;

@SuppressWarnings("serial")
public class VueLaby extends JPanel implements Observer {

	/**
	 * la taille des cases
	 */
	private static final int TAILLE = 30;

	/**
	 * le jeu Laby a afficher
	 */
	JeuLaby laby;

	/**
	 * constructeur de vue laby
	 * 
	 * @param l
	 *            le jeu a afficher
	 */
	public VueLaby(JeuLaby l) {
		this.laby = l;

		// dimension de la vue
		int dimX = l.getTailleX() * TAILLE;
		int dimY = l.getTailleY() * TAILLE;
		Dimension size = new Dimension(dimX, dimY);
		this.setPreferredSize(size);
	}

	/**
	 * affichage du jeu
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		// dessin de l'arene
		int tx = laby.getTailleX();
		int ty = laby.getTailleY();
		for (int i = 0; i < tx; i++)
			for (int j = 0; j < ty; j++)
				g.drawRect(i * TAILLE, j * TAILLE, TAILLE, TAILLE);

		// dessin des joueurs
		for (Personnage joueur : this.laby.personnages) {
			int x = joueur.getX();
			int y = joueur.getY();
			g.fillOval(x * TAILLE, y * TAILLE, TAILLE, TAILLE);
		}

	}

	/**
	 * mise a jour observer
	 */
	@Override
	public void update(Observable o, Object arg) {
		repaint();
	}

}
