package jeux.pacman.iaRandom;

import java.util.ArrayList;
import java.util.List;

import client.random.StructureRandom;

/**
 * client par defaut random qui choisit au hasard parmi les actions donnees
 * 
 * @author vthomas
 * 
 */
public class ClientRandom {

	/**
	 * methode principale
	 * 
	 * @param args
	 *            adresse IP
	 */
	public static void main(String[] args) {
		String[] tab = { "N", "S", "E", "O" };
		
		// creation des actions possibles
		List<String> actions = new ArrayList<>();
		for (String a : tab)
			for (String b : tab)
				actions.add(a +  b);
		String[] array = actions.toArray(new String[0]);

		// creation random
		new StructureRandom(args, array);
	}

}
