package jeux.pacman;

import generic.lanceurSequentiel.LancerJeuSeqReseau;
import jeux.pacman.options.FactoryPacman;

public class MainPacmanReseauServeur {

	/**
	 * lance un serveur reseau a partir d'un jeu labyrinthe
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuSeqReseau lanceur = new LancerJeuSeqReseau(args, new FactoryPacman());
		lanceur.lancerJeu();
	}
}
