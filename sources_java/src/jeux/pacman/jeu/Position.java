package jeux.pacman.jeu;

/**
 * represente une position dans le labyrinthe
 * 
 * @author vthomas
 *
 */
public class Position {

	public int x;
	public int y;
	
	public Position(int i, int j) {
		this.x=i;
		this.y=j;
	}


}
