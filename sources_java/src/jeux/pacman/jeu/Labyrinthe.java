package jeux.pacman.jeu;

/**
 * le labyrinthe a construire
 * 
 */
public class Labyrinthe {

	/**
	 * les murs du labyrnthe
	 */
	boolean murs[][];

	/**
	 * les fruits du labyrinthe
	 */
	private Fruit[][] fruits;

	/**
	 * les departs des personnages
	 */
	private Position departsPacman[];
	private Position departsFantome[];

	/**
	 * creation du labyrinthe
	 * 
	 * @param taille
	 */
	public Labyrinthe(int taille) {
		if (taille < 5)
			taille = 5;
		this.murs = new boolean[taille][taille];
		this.fruits = new Fruit[taille][taille];

		// creation des departs de pacman
		this.departsPacman = new Position[4];
		this.departsPacman[0] = new Position(1, 1);
		this.departsPacman[1] = new Position(taille - 2, taille - 2);
		this.departsPacman[2] = new Position(1, taille - 2);
		this.departsPacman[3] = new Position(taille - 2, 1);

		// creation des departs de fantome
		this.departsFantome = new Position[4];
		this.departsFantome[0] = new Position(2, 2);
		this.departsFantome[1] = new Position(taille - 3, taille - 3);
		this.departsFantome[2] = new Position(2, taille - 3);
		this.departsFantome[3] = new Position(taille - 3, 2);
	}

	/**
	 * @return tailleX
	 */
	public int getTailleX() {
		return this.murs.length;
	}

	/**
	 * @return tailleY
	 */
	int getTailleY() {
		return this.murs[0].length;
	}

	/**
	 * savoir si la case est disponible
	 * 
	 * @param nx
	 *            position x
	 * @param ny
	 *            position y
	 * @return vrai si la case est accessible
	 */
	public boolean etreDisponible(int nx, int ny) {
		if (etreDehors(nx, ny))
			return false;
		return !this.murs[nx][ny];
	}

	/**
	 * savoir si on est dehors
	 * 
	 * @param nx
	 *            position x
	 * @param ny
	 *            position y
	 * @return true si on est hors du laby
	 */
	private boolean etreDehors(int nx, int ny) {
		return (nx < 0) || (ny < 0) || (nx >= this.getTailleX()) || (ny >= this.getTailleY());
	}

	/**
	 * manger un fruit, le supprime et le retourne
	 * 
	 * @param x
	 *            position fruit
	 * @param y
	 *            poqition fruit
	 * @return fruit
	 */
	public Fruit mangerFruit(int x, int y) {
		Fruit fruit = null;
		if (!etreDehors(x, y)) {
			fruit = this.fruits[x][y];
			if (fruit != null)
				this.fruits[x][y] = null;
		}
		return fruit;
	}

	/**
	 * @return liste des fruits
	 */
	public Fruit[][] getFruits() {
		return fruits;
	}

	/**
	 * les departs des pacman
	 */
	public Position getDepartPacman(int num) {
		if ((num < 0) || (num > this.departsPacman.length - 1))
			throw new Error("probleme de num de joueurs ");
		return this.departsPacman[num];
	}

	/**
	 * les departs des fantomes
	 */
	public Position getDepartFantome(int num) {
		if ((num < 0) || (num > this.departsFantome.length - 1))
			throw new Error("probleme de num de joueurs ");
		return this.departsFantome[num];
	}

	/**
	 * retourne le decriptif du labyrinthe
	 * 
	 * @return
	 */
	public String toText() {
		String etat = "";

		// retourne d'abord la taille du jeu
		etat += this.getTailleX();
		etat += ",";
		etat += this.getTailleY();
		etat += JeuPacman.SEP_TEXT;

		// retourne contenu du labyrinthe
		for (int j = 0; j < this.getTailleY(); j++) {
			String ligne = "";
			// ecrit la ligne
			for (int i = 0; i < this.getTailleX(); i++) {
				// si c'est un mur ==> 'X'
				if (!this.etreDisponible(i, j))
					ligne += "X";
				// si c'est un fruit
				else if (this.fruits[i][j] != null) {
					// si c'ets une pacgomme super
					if (this.fruits[i][j].pacgomme()) {
						ligne += "O";
					}
					// sinon, petit pacgomme
					else {
						ligne += "o";
					}
				}
				// il n'y a rien
				else {
					ligne += ".";
				}
			}
			// fin de ligne
			ligne += JeuPacman.SEP_TEXT;
			etat += ligne;
		}
		return etat;
	}

	public void setDepartsPacman(Position[] pacmans) {
		this.departsPacman = pacmans;
	}

	public void setDepartsFantomes(Position[] fantomes) {
		this.departsFantome = fantomes;

	}

}
