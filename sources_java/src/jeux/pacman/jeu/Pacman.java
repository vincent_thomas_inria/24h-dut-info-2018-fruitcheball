package jeux.pacman.jeu;

public class Pacman extends Personnage {

	/**
	 * le temps pendant lequel pacman est agressif
	 */
	private int duree_agressif;

	/**
	 * constructeur de personnage
	 * 
	 * @param id
	 *            id du joueur
	 * @param p
	 *            la position initiale
	 */
	public Pacman(int id, Position p, int nb) {
		super(id, p, nb);
	}

	@Override
	// retourne P pour pacman
	public String getType() {
		return "P";
	}

	@Override
	// pacman qui se deplace et mange des trucs et peut mourir
	public void executerAction(char commande, JeuPacman jeu) {

		// trouve nouvelle case
		int[] nouvelle = this.caseProchaine(commande);
		int nx = nouvelle[0];
		int ny = nouvelle[1];

		// si la case est disponible et qu'elle n'est pas bloquee par autre
		// joueur
		if (jeu.getLaby().etreDisponible(nx, ny)) {
			// si pas de pacman, deplacement possible
			Pacman pacman = jeu.getPacman(nx, ny);
			if (pacman == null) {
				this.x = nx;
				this.y = ny;
			}
		}

		// s'il y a au moins un fantome sur l'arrivee
		Fantome fantome = jeu.getFantome(nx, ny);
		// s'il y a un fantomes
		if (fantome != null) {
			recontrerFantomes(jeu, fantome);
		}

		// s'il y a un fruit sur la case d'arrivee, il le mange
		Fruit f = jeu.getLaby().mangerFruit(this.x, this.y);
		if (f != null) {
			// manger le fruit
			this.points += f.getPoints();

			// si c'est une pacgomme, devient agressif
			if (f.pacgomme())
				this.duree_agressif = FruitGomme.TEMPS_AGGRESSIF + 1;
		}

		// si pacman agressif, devient moins agressif
		if (this.duree_agressif > 0) {
			this.duree_agressif--;
		}

	}

	/**
	 * si pacman rencontre des fantomes
	 * 
	 * @param jeu
	 *            le jeu en cours
	 * @param fantome
	 *            le fantome rencontre
	 */
	private void recontrerFantomes(JeuPacman jeu, Fantome fantome) {
		// si pacman a une pacgomme active
		if (this.etreSousPacgomme()) {
			// pacman mange le fantome
			mangerFantome(jeu, fantome);
		} else {
			// pas de super pacgomme active => le fantome mange pacman
			fantome.mangerPacman(jeu, this);
		}
	}

	/**
	 * quand pacman mange un fantome
	 * 
	 * @param jeu
	 *            le jeu
	 * @param fantome
	 *            le fantome mange
	 */
	void mangerFantome(JeuPacman jeu, Fantome fantome) {
		// met a jour les stats
		this.getStats().manger(fantome.idJoueur);
		fantome.getStats().etreManger(this.idJoueur);

		// le fantome meurt et perd des points
		fantome.mourir(jeu);
		fantome.points -= JeuPacman.POINTS_FANTOME / 2;

		// pacman gagne les points si joueur different
		if (fantome.getId() != this.idJoueur) {
			this.points += JeuPacman.POINTS_FANTOME;
		}
	}

	public boolean etreSousPacgomme() {
		return this.duree_agressif > 0;
	}

	@Override
	public String getTexte() {
		return "P," + this.x + "," + this.y + "," + this.duree_agressif + ","
				+ this.points;
	}

	@Override
	void mourir(JeuPacman jeu) {
		Position depart = jeu.getLaby().getDepartPacman(this.idJoueur);
		Position test=new Position(depart.x,depart.y);
		
		// on cherche une place vide autour
		double num_test=1;
		// tant que la case est pas libre
		while (!jeu.etreDisponible(test.x, test.y))
		{
			num_test += 0.1;
			int hasardX=(int)(3*num_test*Math.random())-(int)num_test;
			int hasardY=(int)(3*num_test*Math.random())-(int)num_test;
			// creer une nouvelle case possible
			test=new Position(depart.x+hasardX,depart.y+hasardY);
		}
		// la case est disponible, met pacman
		this.x=test.x;
		this.y=test.y;
		
			
		// on lui fait manger une pacgomme
		this.duree_agressif = FruitGomme.TEMPS_AGGRESSIF;
	}
}
