package jeux.pacman.jeu;

/**
 * la classe Fruit
 */
public class Fruit {

	/**
	 * les points associes au fruit
	 */
	int points = 1;

	public int getPoints() {
		return this.points;
	}

	/**
	 * @return vrai si c'est une pacgomme
	 */
	public boolean pacgomme() {
		return false;
	}

}
