package jeux.pacman.jeu;

import generic.analyseurOptions.Options;

/**
 * generateur de labyrinthes
 */
public class LabyrintheFactory {

	/**
	 * retourne un labyrinthe pre-construit
	 * 
	 * @param numero
	 *            du labyrinthe numero du labyrinthe
	 * @return le labyrinthe
	 */
	public Labyrinthe getLaby(int numero) {
		switch (numero) {

		case 0:
			return getLabyDefautPetit();

		case 1:
			return getLabyDefaut();

		case 2:
			return getLabyPacman2();

		case 3:
			return getLabyCoins();

		case 4:
			return getLabyPacman();

		case 5:
			return getLabyPacmanVide();

			// labyrinthe par defaut
		default:
			return getLabyDefaut();
		}
	}

	private Labyrinthe getLabyCoins() {
		Labyrinthe l = new Labyrinthe(19);

		// arene
		int taille = l.murs.length;
		for (int i = 0; i < taille; i++) {
			l.murs[0][i] = true;
			l.murs[taille - 1][i] = true;
			l.murs[i][taille - 1] = true;
			l.murs[i][0] = true;
		}

		// murs
		this.tracerMurSymetrique(l, 4, 2, 5, 2);
		this.tracerMurSymetrique(l, 5, 0, 5, 2);
		this.tracerMurSymetrique(l, 2, 4, 2, 5);
		this.tracerMurSymetrique(l, 0, 5, 2, 5);
		this.tracerMurSymetrique(l, 2, 2, 2, 2);
		this.tracerMurSymetrique(l, 4, 4, 5, 4);
		this.tracerMurSymetrique(l, 4, 4, 4, 5);
		this.tracerMurSymetrique(l, 3, 7, 5, 7);
		this.tracerMurSymetrique(l, 7, 3, 7, 5);
		this.tracerMurSymetrique(l, 8, 2, 9, 2);
		this.tracerMurSymetrique(l, 2, 8, 2, 9);
		this.tracerMurSymetrique(l, 3, 9, 3, 9);
		this.tracerMurSymetrique(l, 9, 3, 9, 3);
		this.tracerMurSymetrique(l, 7, 5, 8, 5);
		this.tracerMurSymetrique(l, 5, 7, 5, 8);
		this.tracerMurSymetrique(l, 7, 7, 7, 8);
		this.tracerMurSymetrique(l, 7, 7, 8, 7);

		// departs fantomes au centre
		Position[] fantomes = new Position[4];
		fantomes[0] = new Position(taille / 2 - 1, taille / 2 - 1);
		fantomes[3] = new Position(taille / 2 + 1, taille / 2 - 1);
		fantomes[2] = new Position(taille / 2 - 1, taille / 2 + 1);
		fantomes[1] = new Position(taille / 2 + 1, taille / 2 + 1);
		l.setDepartsFantomes(fantomes);

		// modifie les departs
		Position[] pacmans = new Position[4];
		pacmans[0] = new Position(1, 1);
		pacmans[2] = new Position(1, taille - 2);
		pacmans[3] = new Position(taille - 2, 1);
		pacmans[1] = new Position(taille - 2, taille - 2);
		l.setDepartsPacman(pacmans);

		// fruits partout
		for (int i = 0; i < taille; i++)
			for (int j = 0; j < taille; j++) {
				if (l.murs[i][j] == false)
					l.getFruits()[i][j] = new Fruit();
			}

		// retire dans les departs pacman
		for (int i = 0; i < Options.getOptions().getVal("nbJoueurs"); i++) {
			Position p = l.getDepartPacman(i);
			l.getFruits()[p.x][p.y] = null;
		}

		// ajoute les super pacgommes symetriques
		this.ajouterSuperPacGum(l, 1, 4);
		this.ajouterSuperPacGum(l, 3, 8);
		this.ajouterSuperPacGum(l, 8, 3);
		this.ajouterSuperPacGum(l, 9, 9);

		return l;
	}

	/**
	 * @return labyrinthe par defaut
	 */
	private Labyrinthe getLabyDefaut() {
		Labyrinthe l = new Labyrinthe(13);
		// arene
		int taille = l.murs.length;
		for (int i = 0; i < taille; i++) {
			l.murs[0][i] = true;
			l.murs[taille - 1][i] = true;
			l.murs[i][taille - 1] = true;
			l.murs[i][0] = true;
		}

		// murs centraux
		for (int i = 0; i < 13; i++) {
			l.murs[6][i] = true;
			l.murs[i][6] = true;
		}
		// espaces
		l.murs[3][6] = false;
		l.murs[9][6] = false;
		l.murs[6][3] = false;
		l.murs[6][9] = false;

		// murs centraux
		l.murs[3][3] = true;
		l.murs[9][3] = true;
		l.murs[3][9] = true;
		l.murs[9][9] = true;

		// fruits partout
		for (int i = 0; i < 13; i++)
			for (int j = 0; j < 13; j++) {
				if (l.murs[i][j] == false)
					l.getFruits()[i][j] = new Fruit();
			}

		// retire dans les departs pacman
		for (int i = 0; i < Options.getOptions().getVal("nbJoueurs"); i++) {
			Position p = l.getDepartPacman(i);
			l.getFruits()[p.x][p.y] = null;
		}

		// ajoute les pacgommes
		l.getFruits()[4][4] = new FruitGomme();
		l.getFruits()[8][4] = new FruitGomme();
		l.getFruits()[8][8] = new FruitGomme();
		l.getFruits()[4][8] = new FruitGomme();

		return l;
	}

	/**
	 * @return labyrinthe second par defaut
	 */
	private Labyrinthe getLabyDefautPetit() {
		Labyrinthe l = new Labyrinthe(7);
		// arene
		int taille = l.murs.length;
		for (int i = 0; i < taille; i++) {
			l.murs[0][i] = true;
			l.murs[taille - 1][i] = true;
			l.murs[i][taille - 1] = true;
			l.murs[i][0] = true;
		}

		// murs centraux
		l.murs[3][1] = true;
		l.murs[3][3] = true;
		l.murs[3][5] = true;
		l.murs[1][3] = true;
		l.murs[5][3] = true;

		// fruits partout
		for (int i = 0; i < l.getTailleX(); i++)
			for (int j = 0; j < l.getTailleY(); j++) {
				if (l.murs[i][j] == false)
					l.getFruits()[i][j] = new Fruit();
			}

		// retire dans les departs pacman
		for (int i = 0; i < Options.getOptions().getVal("nbJoueurs"); i++) {
			Position p = l.getDepartPacman(i);
			l.getFruits()[p.x][p.y] = null;
		}

		// ajoute les pacgommes
		l.getFruits()[2][5] = new FruitGomme();
		l.getFruits()[4][1] = new FruitGomme();

		return l;
	}

	/**
	 * @return labyrinthe pacaman
	 */
	private Labyrinthe getLabyPacmanVide() {
		Labyrinthe l = new Labyrinthe(23);

		// arene
		int taille = l.murs.length;
		for (int i = 0; i < taille; i++) {
			l.murs[0][i] = true;
			l.murs[taille - 1][i] = true;
			l.murs[i][taille - 1] = true;
			l.murs[i][0] = true;
		}

		// tracer les murs quart bas gauche
		this.tracerMurSymetrique(l, 3, 3, 3, 7);
		this.tracerMurSymetrique(l, 3, 3, 7, 3);
		this.tracerMurSymetrique(l, 6, 8, 6, 11);
		this.tracerMurSymetrique(l, 8, 6, 11, 6);
		this.tracerMurSymetrique(l, 0, 11, 3, 11);
		this.tracerMurSymetrique(l, 11, 0, 11, 3);

		// cage fantome
		this.tracerMurSymetrique(l, 9, 13, 10, 13);
		this.tracerMurSymetrique(l, 9, 12, 9, 13);

		// fruits partout
		for (int i = 0; i < taille; i++)
			for (int j = 0; j < taille; j++) {
				if (l.murs[i][j] == false)
					l.getFruits()[i][j] = new Fruit();
			}

		// ajoute les super pacgommes symetriques
		this.ajouterSuperPacGum(l, 2, 2);
		this.ajouterSuperPacGum(l, 3, 9);
		// this.ajouterSuperPacGum(l, 7, 7);
		// this.ajouterSuperPacGum(l, 3, 8);

		// modifie les departs
		Position[] pacmans = new Position[4];
		pacmans[0] = new Position(1, 1);
		pacmans[2] = new Position(1, taille - 2);
		pacmans[3] = new Position(taille - 2, 1);
		pacmans[1] = new Position(taille - 2, taille - 2);
		l.setDepartsPacman(pacmans);

		// departs fantomes au centre
		Position[] fantomes = new Position[4];
		fantomes[0] = new Position(taille / 2 - 1, taille / 2 - 1);
		fantomes[3] = new Position(taille / 2 + 1, taille / 2 - 1);
		fantomes[2] = new Position(taille / 2 - 1, taille / 2 + 1);
		fantomes[1] = new Position(taille / 2 + 1, taille / 2 + 1);
		l.setDepartsFantomes(fantomes);

		// retire les pacgums dans les departs pacman
		for (int i = 0; i < Options.getOptions().getVal("nbJoueurs"); i++) {
			Position p = l.getDepartPacman(i);
			l.getFruits()[p.x][p.y] = null;
		}

		return l;
	}

	/**
	 * @return labyrinthe pacaman
	 */
	private Labyrinthe getLabyPacman2() {
		Labyrinthe l = new Labyrinthe(17);

		// arene
		int taille = l.murs.length;
		for (int i = 0; i < taille; i++) {
			l.murs[0][i] = true;
			l.murs[taille - 1][i] = true;
			l.murs[i][taille - 1] = true;
			l.murs[i][0] = true;
		}

		// tracer les murs quart bas gauche
		this.tracerMurSymetrique(l, 2, 2, 2, 4);
		this.tracerMurSymetrique(l, 2, 2, 4, 2);

		this.tracerMurSymetrique(l, 6, 2, 7, 2);
		this.tracerMurSymetrique(l, 2, 6, 2, 7);

		this.tracerMurSymetrique(l, 4, 7, 4, 8);
		this.tracerMurSymetrique(l, 7, 4, 8, 4);

		this.tracerMurSymetrique(l, 4, 4, 4, 4);

		// cage fantome
		this.tracerMurSymetrique(l, 6, 6, 7, 6);
		this.tracerMurSymetrique(l, 6, 6, 6, 7);

		// fruits partout
		for (int i = 0; i < taille; i++)
			for (int j = 0; j < taille; j++) {
				if (l.murs[i][j] == false)
					l.getFruits()[i][j] = new Fruit();
			}

		// ajoute les super pacgommes symetriques
		this.ajouterSuperPacGum(l, 3, 3);
		this.ajouterSuperPacGum(l, 8, 8);

		// modifie les departs
		Position[] pacmans = new Position[4];
		pacmans[0] = new Position(1, 1);
		pacmans[2] = new Position(1, taille - 2);
		pacmans[3] = new Position(taille - 2, 1);
		pacmans[1] = new Position(taille - 2, taille - 2);
		l.setDepartsPacman(pacmans);

		// departs fantomes au centre
		Position[] fantomes = new Position[4];
		fantomes[0] = new Position(taille / 2 - 1, taille / 2 - 1);
		fantomes[3] = new Position(taille / 2 + 1, taille / 2 - 1);
		fantomes[2] = new Position(taille / 2 - 1, taille / 2 + 1);
		fantomes[1] = new Position(taille / 2 + 1, taille / 2 + 1);
		l.setDepartsFantomes(fantomes);

		// retire les pacgums dans les departs pacman
		for (int i = 0; i < Options.getOptions().getVal("nbJoueurs"); i++) {
			Position p = l.getDepartPacman(i);
			l.getFruits()[p.x][p.y] = null;
		}

		return l;
	}

	/**
	 * @return labyrinthe pacaman
	 */
	private Labyrinthe getLabyPacman() {
		Labyrinthe l = new Labyrinthe(23);

		// arene
		int taille = l.murs.length;
		for (int i = 0; i < taille; i++) {
			l.murs[0][i] = true;
			l.murs[taille - 1][i] = true;
			l.murs[i][taille - 1] = true;
			l.murs[i][0] = true;
		}

		// tracer les murs quart bas gauche
		// this.tracerMur(l, 2, 2, 4, 3);
		// this.tracerMur(l, 6, 2, 9, 3);
		// this.tracerMur(l, 2, 5, 4, 5);
		// this.tracerMur(l, 4, 7, 4, 8);

		this.tracerMurSymetrique(l, 6, 11, 7, 11);
		this.tracerMurSymetrique(l, 6, 13, 6, 16);
		this.tracerMurSymetrique(l, 4, 17, 4, 20);
		this.tracerMurSymetrique(l, 6, 20, 6, 21);
		this.tracerMurSymetrique(l, 2, 11, 2, 11);
		this.tracerMurSymetrique(l, 4, 15, 6, 15);
		this.tracerMurSymetrique(l, 4, 12, 4, 13);
		this.tracerMurSymetrique(l, 2, 13, 4, 13);
		this.tracerMurSymetrique(l, 2, 15, 2, 16);
		this.tracerMurSymetrique(l, 2, 18, 4, 18);
		this.tracerMurSymetrique(l, 6, 18, 7, 18);
		this.tracerMurSymetrique(l, 9, 18, 11, 18);
		this.tracerMurSymetrique(l, 2, 20, 2, 20);
		this.tracerMurSymetrique(l, 8, 20, 9, 20);
		this.tracerMurSymetrique(l, 11, 15, 11, 20);
		this.tracerMurSymetrique(l, 8, 16, 9, 16);

		// cage fantome
		// this.tracerMur(l, 9, 9, 10, 9);
		// this.tracerMur(l, 9, 9, 9, 10);
		this.tracerMurSymetrique(l, 9, 13, 10, 13);
		this.tracerMurSymetrique(l, 9, 12, 9, 13);

		// fruits partout
		for (int i = 0; i < taille; i++)
			for (int j = 0; j < taille; j++) {
				if (l.murs[i][j] == false)
					l.getFruits()[i][j] = new Fruit();
			}

		// ajoute les super pacgommes symetriques
		this.ajouterSuperPacGum(l, 3, 3);
		this.ajouterSuperPacGum(l, 7, 1);
		this.ajouterSuperPacGum(l, 7, 7);
		this.ajouterSuperPacGum(l, 3, 8);

		// modifie les departs
		Position[] pacmans = new Position[4];
		pacmans[0] = new Position(1, 1);
		pacmans[2] = new Position(1, taille - 2);
		pacmans[3] = new Position(taille - 2, 1);
		pacmans[1] = new Position(taille - 2, taille - 2);
		l.setDepartsPacman(pacmans);

		// departs fantomes au centre
		Position[] fantomes = new Position[4];
		fantomes[0] = new Position(taille / 2 - 1, taille / 2 - 1);
		fantomes[3] = new Position(taille / 2 + 1, taille / 2 - 1);
		fantomes[2] = new Position(taille / 2 - 1, taille / 2 + 1);
		fantomes[1] = new Position(taille / 2 + 1, taille / 2 + 1);
		l.setDepartsFantomes(fantomes);

		// retire les pacgums dans les departs pacman
		for (int i = 0; i < Options.getOptions().getVal("nbJoueurs"); i++) {
			Position p = l.getDepartPacman(i);
			l.getFruits()[p.x][p.y] = null;
		}

		return l;
	}

	/**
	 * permet d'ajouter une superpacgum de manière symetrique sur le plateau
	 * (ajoute x4)
	 * 
	 * @param l
	 * 
	 * @param i
	 *            coordonnees initiale en x
	 * @param j
	 *            coordonnees initiales en y
	 */
	private void ajouterSuperPacGum(Labyrinthe l, int i, int j) {
		int taille = l.murs.length;
		l.getFruits()[i][j] = new FruitGomme();
		l.getFruits()[taille - 1 - i][j] = new FruitGomme();
		l.getFruits()[taille - 1 - i][taille - 1 - j] = new FruitGomme();
		l.getFruits()[i][taille - 1 - j] = new FruitGomme();
	}

	private void tracerMurSymetrique(Labyrinthe l, int x1, int y1, int x2,
			int y2) {
		int taille = l.murs.length;
		for (int i = x1; i <= x2; i++)
			for (int j = y1; j <= y2; j++) {
				l.murs[i][j] = true;
				l.murs[taille - 1 - i][j] = true;
				l.murs[i][taille - 1 - j] = true;
				l.murs[taille - 1 - i][taille - 1 - j] = true;
			}
	}

}
