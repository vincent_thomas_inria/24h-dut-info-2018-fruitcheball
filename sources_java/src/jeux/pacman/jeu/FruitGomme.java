package jeux.pacman.jeu;

/**
 * represente une pacgomme
 * 
 * @author vthomas
 *
 */
public class FruitGomme extends Fruit {

	/**
	 * la duree de l'agression
	 */
	public static final int TEMPS_AGGRESSIF = 5;

	/**
	 * @return vrai si c'est une pacgomme
	 */
	public boolean pacgomme() {
		return true;
	}

}
