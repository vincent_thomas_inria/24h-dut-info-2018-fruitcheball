package jeux.pacman.options;

import generic.analyseurOptions.Options;
import generic.jeu.JeuSequentiel;
import generic.lanceurSequentiel.FactoryJeuSeq;
import jeux.pacman.jeu.JeuPacman;

public class FactoryPacman extends FactoryJeuSeq {

	/**
	 * les options de jeu
	 */
	private Options opt;

	@Override
	public void initialiserOptions() {
		opt = Options.getOptions();

		// nombre de joueurs
		opt.setOptions("Jeu", "nbJoueurs", 4, "le nombre de joueurs");

		// numero labyrinthe
		opt.setOptions("Jeu", "laby", 1, "le numero du labyrinthe dans [0..4]");

		// affichage
		opt.setOptions("Graphique", "taille", 40, "la taille des cases");
	}

	@Override
	public JeuSequentiel getJeu() {
		return new JeuPacman(opt.getVal("nbJoueurs"));
	}

}
