package jeux.pacman;

import generic.lanceurSequentiel.LancerJeuSeqGraphique;
import jeux.pacman.options.FactoryPacman;

/**
 * permet de lancer le jeu en graphique
 */
public class MainPacmanGraphique {

	/**
	 * creer un lanceur et l'execute
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		LancerJeuSeqGraphique lanceur = new LancerJeuSeqGraphique(args, new FactoryPacman());
		lanceur.bouclerJeu();
	}

}
